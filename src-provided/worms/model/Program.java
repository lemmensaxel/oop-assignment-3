package worms.model;


import java.util.Map;

import worms.gui.game.IActionHandler;
import worms.model.statements.*;
import worms.model.types.Type;

public class Program {

	public Program(IActionHandler handler, Map<String, Type> globals,Statement statement) {
		this.setHandler(handler);
		this.setGlobals(globals);
		this.setStatement(statement);
	}
	
	public void execute(){
		setNumberOfExecutedStatements(0);
		getStatement().execute(getWorm());
	}
	
	public boolean isActionStatement(Statement statement){
		if(statement instanceof Action){
			return true;
		}
		return false;
	}
	
	public boolean wellFormedStatement(Statement statement, boolean foreach){
		if(statement instanceof ForEachLoop)
				foreach = true;
		for(Statement s : statement.getChildren()){
			if(foreach && isActionStatement(s)){
				return false;
			} else if(!isActionStatement(s)){
				wellFormedStatement(s,foreach);
			}
		}
		return true;
	}
	
	public boolean wellFormed(){
		return wellFormedStatement(getStatement(),false);
		
	}
	public int getNumberOfExecutedStatements(){
		return this.numberOfExecutedStatements;
	}
	
	public void setNumberOfExecutedStatements(int number){
		this.numberOfExecutedStatements = number;
		if(this.getNumberOfExecutedStatements() >= 1000)
			getWorm().getWorld().startNextTurn();
	}
	
	private int numberOfExecutedStatements;
	
	public IActionHandler getHandler(){
		return this.handler;
	}
	
	public void setHandler(IActionHandler handler){
		this.handler = handler;
	}
	
	private IActionHandler handler;
	
	public Map<String, Type> getGlobals(){
		return this.globals;
	}
	
	public void setGlobals(Map<String, Type> globals){
		this.globals = globals;
	}
	
	private Map<String, Type> globals;
	
	public Statement getStatement(){
		return this.statement;
	}
	
	public void setStatement(Statement statement){
		this.statement = statement;
	}
	
	private Statement statement;
	
	
	public Worm getWorm() {
		return worm;
	}
		

	public boolean canHaveAsWorm(Worm worm) {
		return (worm != null) && (worm.getProgram() == this)
				&& (this.getWorm() == null);
	}
		
	
	public void setWorm(Worm worm) throws IllegalArgumentException{
		if(!canHaveAsWorm(worm))
			throw new IllegalArgumentException("The provided worm is not a valid worm.");
		this.worm = worm;
	}
		
	
	public void removeWorm() throws IllegalArgumentException {
		if( this.getWorm() == null || !(worm.getProgram() == null) )
			throw new IllegalArgumentException("The provided worm is not a valid worm.");
		setWorm(null);
	}
	
	private Worm worm;
}
