package worms.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import worms.util.Util;

public class Food_test {

	private static final double EPS = Util.DEFAULT_EPSILON;

	private IFacade facade;

	private Random random;

	private World world;

	// X X X X
	// . . . .
	// . . . .
	// X X X X
	private boolean[][] passableMap = new boolean[][] {
			{ false, false, false, false }, { true, true, true, true },
			{ true, true, true, true }, { false, false, false, false } };
	


	@Before
	public void setup() {
		facade = new Facade();
		random = new Random(7357);
		world = facade.createWorld(4.0, 4.0, passableMap, random);
	}
	
	// POSITION
	
	@Test
	public void setXCoordinate_TestCase() {
		if (world.findRandomAdjacentLocation(Food.getRadius())) {
		}
		
		Food food = new Food(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world);
		food.setXCoordinate(3.0);
		
		assertEquals(3.0, food.getCoordinateX(), EPS);
	}
	
	@Test
	public void setYCoordinate_TestCase() {
		if (world.findRandomAdjacentLocation(Food.getRadius())) {
		}
		
		Food food = new Food(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world);
		food.setYCoordinate(11.0);
		
		assertEquals(11.0, food.getCoordinateY(), EPS);
	}
	
	// WORLD
	
	@Test
	public void canHaveAsWorld_LegalCase() {
		if (world.findRandomAdjacentLocation(Food.getRadius())) {
		}
		Food food = new Food(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world);
		
		assertEquals(true, food.canHaveAsWorld(world));
	}
	
	@Test
	public void hasProperWorld_TestCase() {
		if (world.findRandomAdjacentLocation(Food.getRadius())) {
		}
		Food food = new Food(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world);
		
		assertTrue(food.hasProperWorld());
	}
	
	// TERMINATE
	
	@Test
	public void isTerminated_TestCase() {
 
		
		if (world.findRandomAdjacentLocation(Food.getRadius())) {
		}
		Food food = new Food(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world);
		
		assertFalse(food.isTerminated());
	}

}
