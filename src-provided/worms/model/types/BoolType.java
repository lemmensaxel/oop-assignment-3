package worms.model.types;

public class BoolType extends Type{
	
	public BoolType(boolean state){
		setState(state);
	}
	
	public BoolType(){
		this(false);
	}
	
	public boolean getState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
		
	}

	private boolean state;

	@Override
	public String toString() {
		return new Boolean(getState()).toString();
	}

	@Override
	public boolean equals(Object other) {
		if ((other == null) || (getClass() != other.getClass()))
			return false;
		BoolType otherBool = (BoolType) other;
		if (! (this.getState() == otherBool.getState()) )
				return false;
		return true;
	}
}
