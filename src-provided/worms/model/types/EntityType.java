package worms.model.types;

public class EntityType extends Type {
	
	public EntityType(Object object){
		setObject(object);
	}
	
	public EntityType(){
		this(null);
	}
	
	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	private Object object;

	@Override
	public String toString() {
		return getObject().toString();
	}
	
	@Override
	public boolean equals(Object other) {
		if ((other == null) || (getClass() != other.getClass()))
			return false;
		EntityType otherObject = (EntityType) other;
		if(this.getObject() != null && otherObject.getObject() != null)
			if ( (this.getObject().equals(otherObject.getObject())) )
					return true;
		return false;
	}
}