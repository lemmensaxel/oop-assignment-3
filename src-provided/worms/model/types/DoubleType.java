package worms.model.types;

public class DoubleType extends Type{
	
	public DoubleType(double value){
		setValue(value);
	}
	
	public DoubleType(){
		this(0);
	}
	
	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	private double value;

	@Override
	public String toString() {
		return new Double(getValue()).toString();
	}
	
	@Override
	public boolean equals(Object other) {
		if ((other == null) || (getClass() != other.getClass()))
			return false;
		DoubleType otherDouble = (DoubleType) other;
		if (! (this.getValue() == otherDouble.getValue()) )
				return false;
		return true;
	}
}
