package worms.model.types;

public abstract class Type {
	
	@Override 
	public abstract String toString();
	
	@Override
	public abstract boolean equals(Object other);
		

}
