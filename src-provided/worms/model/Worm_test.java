package worms.model;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import worms.util.Util;


public class Worm_test {
	

	private static final double EPS = Util.DEFAULT_EPSILON;

	private IFacade facade;

	private Random random;

	private World world;

	// X X X X
	// . . . .
	// . . . .
	// X X X X
	private boolean[][] passableMap = new boolean[][] {
			{ false, false, false, false }, { true, true, true, true },
			{ true, true, true, true }, { false, false, false, false } };
	


	@Before
	public void setup() {
		facade = new Facade();
		random = new Random(7357);
		world = facade.createWorld(4.0, 4.0, passableMap, random);
	}
	
	// POSITION
	
	@Test
	public void isValidCoordinate_LegalCase() {

		
		assertEquals(true, Worm.isValidCoordinate(1.0));
	}
	
	@Test
	public void isValidCoordinate_IllegalCase() {
		
		assertEquals(true, Worm.isValidCoordinate(-1.0));
	}
	
	@Test
	public void setCoordinateX_LegalCase() throws IllegalArgumentException {
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		worm.setXCoordinate(8.0);
		
		assertEquals(8.0, worm.getCoordinateX(), EPS);
	}

	
	@Test
	public void setCoordinateY_LegalCase() throws IllegalArgumentException {
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		worm.setYCoordinate(10.0);
		
		assertEquals(10.0, worm.getCoordinateY(), EPS);
	}
	
	//DIRECTION
	
	@Test
	public void getDirection_Case() {
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		assertEquals(0.5, worm.getDirection(), EPS);
	}
	
	
	@Test
	public void isValidDirection_LegalCase() {
		
		assertEquals(true, Worm.isValidDirection(1.0));
	}
	
	
	@Test
	public void setDirection_LegalCase() {
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		worm.setDirection(Math.PI/8);
		
		assertEquals(Math.PI/8, worm.getDirection(), EPS);
	}
	
	//RADIUS
	
	@Test
	public void isValidRadius_LegalCase() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		assertTrue(worm.isValidRadius(1.0));
	}
	
	@Test
	public void isValidRadius_IllegalCase() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		assertFalse(worm.isValidRadius(0.21));
	}
	
	@Test
	public void isValidRadius_IllegalCase2() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		assertFalse(worm.isValidRadius(-0.25));
	}
	
	@Test
	public void setRadius_LegalCase() throws IllegalArgumentException {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		worm.setRadius(0.53);
		
		assertEquals(0.53, worm.getRadius(), EPS);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void setRadius_IllegalCase() throws IllegalArgumentException {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		worm.setRadius(-1.0);
		
	}
	
	//MASS
	
	@Test
	public void getMass_Case() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		assertEquals(556, worm.getMass(), 1);
		
	}
	
	// ACTION POINTS
	
	@Test
	public void getMaximumActionPoints_Case() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		assertEquals(556,worm.getMaximumActionPoints());
		
		
	}
	
	@Test
	public void getCurrentActionPoints_Case() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		assertEquals(556, worm.getCurrentActionPoints());
	}
	
	@Test
	public void setCurrentActionPoints_LegalCase1() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		worm.setCurrentActionPoints(500);
		
		assertEquals(500, worm.getCurrentActionPoints());
		
	}

	@Test
	public void setCurrentActionPoints_LegalCase3() { 
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		worm.setCurrentActionPoints(-500);
		assertEquals(0, worm.getCurrentActionPoints());
	}
	
	//NAME
	
	@Test
	public void getName_Case() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		assertEquals("Kiki", worm.getName());
	}
	
	@Test
	public void isValidName_LegalCase() {
		
		assertEquals(true, Worm.isValidName("Axel"));
	}
	
	@Test
	public void isValidName_IllegalCase1() {
		
		assertEquals(false, Worm.isValidName("axel"));
	}
	
	@Test
	public void isValidName_IllegalCase2() {
		
		assertEquals(false, Worm.isValidName("Axel^$$"));
	}
	
	@Test
	public void isValidName_IllegalCase3() {
		
		assertEquals(true, Worm.isValidName("Ax"));
	}
	
	@Test
	public void isValidName_IllegalCase4() {
		
		assertEquals(false, Worm.isValidName("34254"));
	}

	@Test
	public void isValidName_IllegalCase5() {
		
		assertEquals(true, Worm.isValidName("A\"xe\"l"));
	}
	
	@Test
	public void isValidName_IllegalCase6() {
		
		assertEquals(true, Worm.isValidName("Axel45678"));
	}
	
	@Test
	public void isValidName_IllegalCase7() {
		
		assertEquals(false, Worm.isValidName("^$^"));
	}
	
	@Test
	public void isValidName_IllegalCase8() {
		
		assertEquals(false, Worm.isValidName("A"));
	}
	
	@Test
	public void isValidName_IllegalCase9() {
		
		assertEquals(false, Worm.isValidName(""));
	}
	
	@Test
	public void setName_LegalCase() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		worm.setName("ObjectOrientedProgrammer");
		
		assertEquals("ObjectOrientedProgrammer", worm.getName());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void setName_IllegalCase() throws IllegalArgumentException {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		worm.setName("objectOrientedProgrammer");
	}
	
	//MOVING
	
	@Test
	public void hasEnoughActionPoints_LegalCase() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		worm.setCurrentActionPoints(500);
		assertEquals(true, worm.hasEnoughActionPoints(1));
		
	}
	
	@Test
	public void hasEnoughActionPoints_IllegalCase() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		worm.setCurrentActionPoints(0);
		assertEquals(false, worm.hasEnoughActionPoints(5));
		
	}
	
	// TURN
	
	@Test
	public void turn_LegalCase() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		worm.setCurrentActionPoints(500);
		double direction = worm.getDirection();
		worm.turn(2);
		assertEquals(direction+2, worm.getDirection(), EPS);
		assertEquals(481, worm.getCurrentActionPoints(), EPS);
		
	}
	
	@Test
	public void canTurn_LegalCase() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		worm.setCurrentActionPoints(500);
		assertEquals(true, worm.canTurn(2));
	}
	
	@Test
	public void canTurn_IllegalCase() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		worm.setCurrentActionPoints(0);
		assertEquals(false, worm.canTurn(2));
	}
	
	// JUMP
	
	@Test
	public void getForce_Case() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		worm.setCurrentActionPoints(100);
		assertEquals(5953.104428, worm.getForce(), EPS);
		
	}
	
	@Test
	public void getVelocity_Case() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		assertEquals(7.403, worm.getVelocity(), EPS);
	}
	
	@Test
	public void canJump_LegalCase() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		worm.setCurrentActionPoints(100);
		assertEquals(true, worm.canJump());
	}
	
	@Test
	public void canJump_IllegalCase() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		assertEquals(true, worm.canJump());
		
	}
	
	@Test
	public void canJump_IllegalCase2() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		assertEquals(true, worm.canJump());
		
	}


	
	@Test
	public void canJump_IllegalCase3() {
		if(world.findRandomAdjacentLocation(0.50)){
		}
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		assertEquals(true, worm.canJump());
		
	}
	
	// PROGRAM
	
	@Test
	public void canHaveAsProgram_LegalCase() {
		if(world.findRandomAdjacentLocation(0.50)){
		}
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		Program prog = new Program(null, null, null);
		assertEquals(true, worm.canHaveAsProgram(prog));
		
	}
	
	@Test
	public void canHaveAsProgram_IllegalCase() {
		if(world.findRandomAdjacentLocation(0.50)){
		}
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		Program prog = null;
		assertEquals(false, worm.canHaveAsProgram(prog));
		
	}
	
	@Test
	public void hasProperProgram_Case() {
		if(world.findRandomAdjacentLocation(0.50)){
		}
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		assertEquals(false, worm.hasProperProgram());
		
	}

}