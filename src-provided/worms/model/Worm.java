package worms.model;

import java.util.ArrayList;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class of worms.
 * 
 * @invar	Each worm must have a valid x-coordinate.
 * 			| isValidCoordinate(getCoordinateX())
 * @invar 	Each worm must have a valid y-coordinate.
 * 			| isValidCoordinate(getCoordinateY())
 * @invar	Each worm must have a valid direction.
 * 			| isValidDirection(getDirection())
 * @invar	Each worm must have a valid radius.
 * 			| isValidRadius(getRadius())
 * @invar	Each worm must have a valid name.
 * 			| isValidName(getName())
 * @invar  	Each worm must have a proper team.
 *       	| hasProperTeam()
 * @invar  	Each worm must have a proper world.
 *       	| hasProperWorld()
 * @invar  	Each worm must have a proper program.
 *       	| hasProperProgram()
 *
 * @version	3.0
 * @author Axel Lemmens & Maarten Rimaux 
 *
 */
public class Worm extends GameObject {
	
	//CONSTRUCTOR
	
	/**
	 * @param 		xCoordinate
	 * 				The provided value for the x-coordinate in meters.
	 * @param 		yCoordinate
	 * 				The provided value for the y-coordinate in meters.
	 * @param 		direction
	 * 				The provided value for the direction in radians.
	 * @param 		radius
	 * 				The provided value for the radius in meter.
	 * @param 		name
	 * 				The provided string for the name.
	 * @param 		world
	 * 				The provided world for this worm.
	 * @param 		program
	 * 				The provided program for this worm.
	 * @pre			The provided direction must be a valid value.
	 * 				| isValidDirection(direction)
	 * @effect		The new value of the x-coordinate is set to the provided value "xCoordinate"
	 * 				| setXCoordinate(xCoordinate)
	 * @effect		The new value of the y-coordinate is set to the provided value "yCoordinate"
	 * 				| setYCoordinate(yCoordinate)
	 * @effect		The new value of the direction is set to the provided value "direction"
	 * 				| setDirection(direction)
	 * @effect		The new value of the radius is set to the provided value "radius"
	 * 				| setRadius(radius)
	 * @effect		The new value of the name is set to the provided string "name"
	 * 				| setName(name)
	 * @effect		the new value of the action points is set to the maximum action points
	 * 				| setCurrentActionPoints(getMaximumActionPoints())
	 * @effect		the new value of the hit points is set to the maximum hit points
	 * 				| setCurrentHitPoints(getMaximumHitPoints())
	 * @effect		The new world of this worm is set to the provided "world"
	 * 				| setWorld(world)
	 * @effect		The new program of this worm is set to the provided "program"
	 * 				| setProgram(program)
	 * @effect		The next weapon is selected
	 * 				| new.getSelectedWeapon = getWeapons().get(0)
	 * @post   		The number of worms for the given world is
	 *         		incremented by 1.
	 *       		| (new world).getWorms().size() == world.getWorms().size() + 1
	 * @post   		The given world has this new worm as its very last worm.
	 *      		| (new world).getWorms().get(world.getWorms().size()+1) == this
	 * @post   		The given program is the program of this worm.
	 *       		| new.getProgram() == program
	 * @post   		If The given program is effective then the given program 
	 * 				has this worm as its worm.
	 *      		| (new program).getWorm() == this
	 * @post		The number of weapons for this worm is incremented by 1 with the rifle gun.
	 * 				| (new worm).getWeapons().size() == this.getWeapons().size()+1
	 * @post		The number of weapons for this worm is incremented by 1 with the bazooka gun.
	 * 				| (new worm).getWeapons().size() == this.getWeapons().size()+1
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided value is not a valid coordinate.
	 * 				| ! isValidCoordinate(xCoordinate)
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided value is not a valid coordinate.
	 * 				| ! isValidCoordinate(yCoordinate)
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided value is not a valid radius.
	 * 				| ! isValidRadius(radius)
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided value is not a valid name.
	 * 				| ! isValidName(name)
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided world is not a valid world.
	 * 				| ! canHaveAsWorld(world)
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided program is not a valid program.
	 * 				| ! canHaveAsProgram(program)
	 */
	public Worm(double xCoordinate, double yCoordinate, World world, double direction, double radius, String name, Program program) throws IllegalArgumentException {
		super(xCoordinate, yCoordinate, world);
		this.setDirection(direction);
		this.setRadius(radius);
		this.setName(name);
		this.setCurrentActionPoints(getMaximumActionPoints());
		this.setCurrentHitPoints(getMaximumHitPoints());
		this.setTeam(null);
		this.setProgram(program);
		if(program != null)
			program.setWorm(this);
		@SuppressWarnings("unused")
		Rifle rifle = new Rifle(this);
		@SuppressWarnings("unused")
		Bazooka bazooka = new Bazooka(this);
		selectNextWeapon();
	}

	
	
	// ORIENTATION / DIRECTION
	
	/**
	 * A method to return the current value of the angel of this worm.
	 */
	@Basic @Raw
	public double getDirection() {
		return direction;
	}
	
	/**
	 * A method to set the direction of this worm to the provided value.
	 * 
	 * @param 		direction
	 * 				The new direction for the orientation of this worm.
	 * @pre			The provided value must be a valid value.
	 * 				| isValidDirection(direction)
	 * @post		The direction of this worm is set to the provided value.
	 * 				| new.getDirection() == direction
	 */
	@Raw
	public void setDirection(double direction) { 
		assert(isValidDirection(direction));
		this.direction = direction;
	}
	
	/**
	 * A method to check whether a direction is valid.
	 * 
	 * @param 		direction 
	 * 				The direction to check.
	 * @return 		The result should be a valid number.
	 * 				| result == !double.isNaN(direction) 
	 * 				| && (direction < Double.POSITIVE_INFINITY) 
	 * 				| && (direction > Double.NEGATIVE_INFINITY)
	 */
	public static boolean isValidDirection(double direction){
		return (!Double.isNaN(direction) 
				&& (direction < Double.POSITIVE_INFINITY) 
				&& (direction > Double.NEGATIVE_INFINITY));	
	}
	
	/**
	 * Variable registering the direction for the orientation of this worm.
	 */
	private double direction;
	
	// RADIUS
	
	/**
	 * A method to return the current radius of this worm expressed in meters.
	 */
	@Basic @Raw
	public double getRadius() {
		return radius;
	}
	
	/**
	 * A method to return the lower bound radius of this worm expressed in meters.
	 */
	@Basic @Raw @Immutable
	public double getLowerBoundRadius() {
		return lowerBoundRadius;
	}
	
	/**
	 * A method that sets the radius of this worm to the provided value.
	 * 
	 * @param 		radius
	 * 				The new radius of this worm.
	 * @post 		The radius of this worm is set to the provided value.
	 * 				| new.getRadius() == radius
	 * @post		IllegalArgumentException
	 * 				The method must throw an exception when the provided value is not a valid radius.
	 * 				| !isValidRadius(radius)
	 */
	@Raw
	public void setRadius(double radius) throws IllegalArgumentException {
		if (! isValidRadius(radius))
			throw new IllegalArgumentException();
		this.radius = radius;
	}
	
	/**
	 * A method to check whether a given value for radius is valid.
	 * 
	 * @param		radius
	 * 				The radius to check.
	 * @return		The result should be larger or equal to the lower bound of the radius.
	 * 				| result ==  (radius >= getLowerBoundRadius())
	 * @return 		The result must be a valid number.
	 * 				| result == double.isNaN(radius) 
	 * 				| && (radius < Double.POSITIVE_INFINITY)
	 * 				| && (radius > Double.NEGATIVE_INFINITY))
	 */
	public Boolean isValidRadius(double radius) {
		return (radius >= this.getLowerBoundRadius())
				&& (!Double.isNaN(radius) 
				&& (radius < Double.POSITIVE_INFINITY)
				&& (radius > Double.NEGATIVE_INFINITY));
	}
	
	/**
	 * Variable registering the radius of this worm.
	 */
	private double radius;
	
	/**
	 * Variable registering the lower bound radius of this worm.
	 */
	private final double lowerBoundRadius = 0.25;
	
	//  MASS
	
	/**
	 * A method to return the mass of a worm expressed in kilogram.
	 * 
	 * @return 		The mass of this worm.
	 * 				| result ==	
	 * 				| this.getDensity()*((4.0/3.0)*(Math.PI)*(Math.pow(this.getRadius(),3)))
	 */
	@Basic
	public double getMass() {
		return (this.getDensity()*((4.0/3.0)*(Math.PI)*(Math.pow(this.getRadius(),3))));
	}
	
	/**
	 * A method to return the density of this worm.
	 */
	@Basic @Immutable
	public double getDensity(){
		return 1062.0;
	}
	
	// ACTION POINTS
	
	/**
	 * A method to return the maximum amount of action points of this worm.
	 * 
	 * @return 		The maximum amount of action of this worm.
	 * 				| result == (int)Math.round(this.getMass())
	 */
	@Basic
	public int getMaximumActionPoints() {
		return (int) Math.round(this.getMass());
	}
	
	/**
	 * A method to return the minimum amount of action points of this worm.
	 */
	@Basic	@Immutable
	public int getMinimumActionPoints() {
		return 0;
	}
	
	/**
	 * A method to return the current amount of action points of this worm.
	 */
	@Basic @Raw
	public int getCurrentActionPoints() {
		return currentActionPoints;
	}
	
	/**
	 * A method to set the current action points on this worm to the provided value.
	 * 
	 * @param 		currentPoints
	 * 				The new current points for the action points of this worm.
	 * @post		If currentPoints is smaller or equal to the maximum value of points 
	 * 				and if currentPoints is larger or equal to the minimum value of points
	 * 				then currentPoints is set to the provided value.
	 * 				|if ((currentPoints <= getMaximumActionPoints()) 
	 * 				| && (currentPoints >= getMinimumActionPoints()))
	 * 				|	then new.getCurrentActionPoints() == currentPoints
	 * @post		If currentPoints is bigger than the maximum amount of action points, 
	 * 				currentPoints is set to the maximum amount of points.
	 * 				|if (currentPoints > getMaximumActionPoints())
	 * 				| 	then new.getCurrentActionPoints() == this.getMaximumActionPoints()
	 * @post		If the currentPoint is smaller than the minimum amount of action points, 
	 * 				currentPoints is set to the minimum amount of points.
	 * 				|if (currentPoints < getMinimumActionPoints())
	 * 				|	then new.getCurrentActionPoints() == this.getMinimumActionPoints()
	 * @post		If The current action points are equal to the minimum then the next turn in the
	 * 				world is started.
	 * 				| if ( getCurrentActionPoints() == getMinimumActionPoints() )
	 * 				| 	then getWorld().startNextTurn()
	 */
	@Raw
	public void setCurrentActionPoints(int currentPoints) {
		if ( (currentPoints <= getMaximumActionPoints()) && (currentPoints >= getMinimumActionPoints()) )
				this.currentActionPoints = currentPoints;
		
		else if (currentPoints > getMaximumActionPoints()) {
				this.currentActionPoints = getMaximumActionPoints();
				}
		
		else if (currentPoints < getMinimumActionPoints()) {
				this.currentActionPoints =  getMinimumActionPoints();
				}
		if(getCurrentActionPoints() == getMinimumActionPoints())
			getWorld().startNextTurn();
	}
	
	/**
	 * Variable registering the current action points of this worm.
	 */
	private int currentActionPoints;
	
	// HIT POINTS
	
	/**
	 * A method to return the maximum amount of hit points of this worm.
	 * 
	 * @return 		The maximum amount of hit points of this worm.
	 * 				| result == (int)Math.round(this.getMass())
	 */
	@Basic
	public int getMaximumHitPoints() {
		return (int) Math.round(this.getMass());
	}
	
	/**
	 * A method to return the minimum amount of hit points of this worm.
	 */
	@Basic	@Immutable
	public int getMinimumHitPoints() {
		return 0;
	}
	
	/**
	 * A method to return the current amount of hit points of this worm.
	 */
	@Basic @Raw
	public int getCurrentHitPoints() {
		return currentHitPoints;
	}
	
	/**
	 * A method to set the current hit points on this worm to the provided value.
	 * 
	 * @param 		currentPoints
	 * 				The new current points for the hit points of this worm.
	 * @post		If currentPoints is smaller or equal to the maximum value of points 
	 * 				and if currentPoints is larger or equal to the minimum value of points
	 * 				then currentPoints is set to the provided value.
	 * 				|if ((currentPoints <= getMaximumHitPoints()) 
	 * 				| && (currentPoints >= getMinimumHitPoints()))
	 * 				|	then new.getCurrentHitPoints() == currentPoints
	 * @post		If currentPoints is bigger than the maximum amount of hit points, 
	 * 				currentPoints is set to the maximum amount of points.
	 * 				|if (currentPoints > getMaximumHitPoints())
	 * 				| 	then new.getCurrentHitPoints() == this.getMaximumHitPoints()
	 * @post		If the currentPoint is smaller than the minimum amount of hit points, 
	 * 				currentPoints is set to the minimum amount of points.
	 * 				|if (currentPoints < getMinimumHitPoints())
	 * 				|	then new.getCurrentHitPoints() == this.getMinimumHitPoints()
	 */
	@Raw
	public void setCurrentHitPoints(int currentPoints) {
		if ( (currentPoints <= getMaximumHitPoints()) && (currentPoints >= getMinimumHitPoints()) )
				this.currentHitPoints = currentPoints;
		
		else if (currentPoints > getMaximumHitPoints()) {
				this.currentHitPoints = getMaximumHitPoints();
				}
		
		else if (currentPoints < getMinimumHitPoints()) {
				this.currentHitPoints =  getMinimumHitPoints();
				}
	}
	
	/**
	 * Variable registering the current hit points of this worm.
	 */
	private int currentHitPoints;
	
	// NAME
	
	/**
	 * A method that returns the current name of this worm.
	 */
	@Basic @Raw
	public String getName() {
		return name;
	}
	
	/**
	 * A method to set the name to a given string.
	 * 
	 * @param		name
	 * 				The new name of this worm.
	 * @post		The name of this worm is set to the provided value.
	 * 				| new.getName() == name
	 * @throws		IllegalArgumentException
	 * 				The method throws the exception when the provided string is not valid.
	 * 				| (! isValidName(name))
	 */
	@Raw
	public void setName(String name) throws IllegalArgumentException {
		if ( ! isValidName(name))
			throw new IllegalArgumentException();
		this.name = name;
		
	}
	
	/**
	 * A method to check whether a given string for name is valid.
	 * 
	 * @param 		name
	 * 				The name to check.
	 * @return		The result should be a string starting with an upper case letter, 
	 * 				at least two characters long and contains only letters, numbers, quotes and spaces. 
	 * 				| result ==  (string.matches("^[A-Z][a-zA-Z0-9\\s\'\"]{1,}")
	 */
	public static boolean isValidName(String name) {
		return name.matches("^[A-Z][a-zA-Z0-9\\s\'\"]{1,}");
	}
	
	/**
	 * Variable registering the name of this worm.
	 */
	private String name;
	
	//MOVING
	
	/**
	 * A method to return the found x-coordinate in the method move.
	 */
	@Basic @Raw
	public double getFoundXCoordinate(){
		return foundXCoordinate;
	}
	
	/**
	 * A method to return the found y-coordinate in the method move.
	 */
	@Basic @Raw
	public double getFoundYCoordinate(){
		return foundYCoordinate;
	}
	
	/**
	 * A method to set the found x-coordinate in the method move.
	 * 
	 * @param	foundXCoordinate
	 * 			The found x-coordinate to set.
	 * @post	The new found x-coordinate is set to the provided value.
	 * 			| new.getFoundXCoordinate() == foundXCoordinate
	 * @throws 	IllegalArgumentException
	 * 			An exception is thrown when the coordinate is not valid.
	 * 			| ! isValidCoordinate(foundXCoordinate)
	 */
	@Raw
	public void setFoundXCoordinate(double foundXCoordinate) throws IllegalArgumentException{
		if(!isValidCoordinate(foundXCoordinate))
			throw new IllegalArgumentException();
		this.foundXCoordinate = foundXCoordinate;
	}
	
	/**
	 * A method to set the found y-coordinate in the method move.
	 * 
	 * @param	foundYCoordinate
	 * 			The found y-coordinate to set.
	 * @post	The new found y-coordinate is set to the provided value.
	 * 			| new.getFoundYCoordinate() == foundYCoordinate
	 * @throws 	IllegalArgumentException
	 * 			An exception is thrown when the coordinate is not valid.
	 * 			| ! isValidCoordinate(foundYCoordinate)
	 */
	@Raw
	public void setFoundYCoordinate(double foundYCoordinate) throws IllegalArgumentException{
		if(!isValidCoordinate(foundYCoordinate))
			throw new IllegalArgumentException();
		this.foundYCoordinate = foundYCoordinate;
	}
	
	/**
	 * A method to check whether this worm can move.
	 * 
	 * @post	If the result is true and only if it is true,
	 * 			the found x-coordinate is set to the found
	 * 			location.
	 * 			| new.getFoundXCoordinate()
	 *  @post	If the result is true and only if it is true,
	 * 			the found y-coordinate is set to the found
	 * 			location.
	 * 			| new.getFoundYCoordinate
	 * @return	The result is true if the found location is 
	 * 			passable or adjacent to impassable terrain and
	 * 			if the worm has enough action points to move,
	 * 			otherwise false.
	 * 			| for each distance in 0.1 .. getRadius():
	 * 			|	for each divergence in 0 .. 0.7875:
	 * 			|		if( the location is passable or Adjacent)
	 *			|			then if( hasEnoughActionPoints() )			
	 *			|					then setFoundedXCoordinate( (Math.cos(getDirection()+divergence)*distance) + getCoordinateX() )
	 *			|					&&	 setFoundedYCoordinate( (Math.sin(getDirection()+divergence)*distance) + getCoordinateY() )
	 *			| result == false
	 */	
	public boolean canMove(){
	    for(double distance = getRadius(); distance >= 0.1; distance -= 0.001){
	    	for(double divergence = 0; divergence <= 0.7875; divergence += 0.0175){
				if( (getWorld().isPassable(
						( (Math.cos(getDirection()+divergence)*distance) + getCoordinateX() ), 
						( (Math.sin(getDirection()+divergence)*distance) + getCoordinateY() ), getRadius())) ||
					 getWorld().isAdjacentToImpassableTerrain(
					    ( (Math.cos(getDirection()+divergence)*distance) + getCoordinateX() ),
					    ( (Math.sin(getDirection()+divergence)*distance) + getCoordinateY() ), getRadius())) {
					double tempSlope = Math.atan(( ((Math.sin(getDirection()+divergence)*distance) + getCoordinateY()) - this.getCoordinateY()) / 
												 ( ((Math.cos(getDirection()+divergence)*distance) + getCoordinateX()) - this.getCoordinateX()));
					if(hasEnoughActionPoints(tempSlope)){
						setFoundXCoordinate( (Math.cos(getDirection()+divergence)*distance) + getCoordinateX() );
						setFoundYCoordinate( (Math.sin(getDirection()+divergence)*distance) + getCoordinateY() );
						return true;
					}
				}
				if( (getWorld().isPassable(
						( (Math.cos(Math.abs(getDirection()-divergence))*distance) + getCoordinateX() ), 
						( (Math.sin(Math.abs(getDirection()-divergence))*distance) + getCoordinateY() ), getRadius())) ||
					 getWorld().isAdjacentToImpassableTerrain(
					    ( (Math.cos(Math.abs(getDirection()-divergence))*distance) + getCoordinateX() ),
					    ( (Math.sin(Math.abs(getDirection()-divergence))*distance) + getCoordinateY() ), getRadius())) {
					double tempSlope = Math.atan(( ((Math.sin(Math.abs(getDirection()-divergence))*distance) + getCoordinateY()) - this.getCoordinateY()) / 
												 ( ((Math.cos(Math.abs(getDirection()-divergence))*distance) + getCoordinateX()) - this.getCoordinateX()));
					if(hasEnoughActionPoints(tempSlope)){
						setFoundXCoordinate( (Math.cos(Math.abs(getDirection()-divergence))*distance) + getCoordinateX() );
						setFoundYCoordinate( (Math.sin(Math.abs(getDirection()-divergence))*distance) + getCoordinateY() );
						return true;
					}
				}
			}
		}
		return false;
	}
	
	/**
	 *  A method to check whether the worm has enough action points to move.
	 *  
	 * @param 	slope
	 * 			The slope to check.
	 * @return	The result is true if the worm has enough action points to move.
	 * 			| result == ( (this.getCurrentActionPoints() - (Math.abs(Math.cos(slope)) + (Math.abs(4*Math.sin(slope)))))
	 * 			|			>= getMinimumActionPoints() )
	 * 			|			
	 */
	public boolean hasEnoughActionPoints(double slope) {
		return (this.getCurrentActionPoints() - (Math.abs(Math.cos(slope)) + ( Math.abs( 4*Math.sin(slope))))) >= getMinimumActionPoints();
	}
	
	/**
	 *  A method to move the worm.
	 *  
	 * @effect 	If the worm can move the x-coordinate is set to the found x-coordinate
	 * 			|  setXCoordinate(getFoundXCoordinate())
	 * @effect 	If the worm can move the y-coordinate is set to the found y-coordinate
	 * 			|  setYCoordinate(getFoundYCoordinate())
	 * 			|  setXCoordinate(getfoundXCoordinate())
	 * @effect 	If the worm can move the y-coordinate is set to the found y-coordinate
	 * 			|  setYCoordinate(getfoundYCoordinate())
	 * @post	If the worm can move the action points are decreased
	 * 			| new.getCurrentActionPoints == (int)Math.ceil( this.getCurrentActionPoints() 
	 * 			|		-	( Math.abs( Math.cos(slope) ) + ( Math.abs( 4*Math.sin(slope)))))
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the worm cannot move.
	 * 			| ! canMove()
	 */
	public void move() throws IllegalArgumentException {
		if(!canMove())
			throw new IllegalArgumentException();
		double slope = Math.atan((this.getCoordinateY()-getFoundYCoordinate())/(this.getCoordinateX()-getFoundXCoordinate()));
		setXCoordinate(getFoundXCoordinate());
		setYCoordinate(getFoundYCoordinate());		
		setCurrentActionPoints( (int)Math.ceil( getCurrentActionPoints() - ( Math.abs( Math.cos(slope) ) + ( Math.abs( 4*Math.sin(slope) ) ) ) ) );
	}
	
	/**
	 * Variable registering the current found x-coordinate of this worm.
	 */
	private double foundXCoordinate;
	
	/**
	 * Variable registering the current found y-coordinate of this worm.
	 */
	private double foundYCoordinate;
	
	// FALL
	
	/**
	 * A method to check whether this worm can fall.
	 * 
	 * @return the result is true if this worm is on passable terrain but not
	 * 		   Adjacent to impassable terrain.
	 * 		   | result == this.getWorld().isPassable(getCoordinateX(), getCoordinateY(), getRadius())
	 * 		   |	&& 	!this.getWorld().isAdjacentToImpassableTerrain(getCoordinateX(), getCoordinateY(), getRadius())
	 */	
	public boolean canFall() {
		return this.getWorld().isPassable(getCoordinateX(), getCoordinateY(), getRadius()) 
				&& !this.getWorld().isAdjacentToImpassableTerrain(getCoordinateX(), getCoordinateY(), getRadius());
	}
	
	/**
	 * A method to let this worm fall.
	 * 
	 * @post	If this worm can fall and this worm is not terminated
	 * 			then this worm will fall until it finds a location that
	 * 			is adjacent to impassable terrain.
	 * 			| while ( getWorld.isPassable() && !getWorld().isAdjacent() && !this.isTerminated() )
	 *			|		setYCoordinate(getCoordinateY()-0.001)
	 *			|		if (this.getCoordinateY < 0)
	 *			|			then this.terminate()
	 * @post	If this worm has fallen, his hit points are decreased.
	 *			| setCurrentHitPoints(getCurrentHitPoints()-3*(int)Math.floor(oldCoordinateY-getCoordinateY()))
	 * @throws 	IllegalArgumentException
	 * 			An exception is thrown when the worm cannot fall.
	 * 			| ! canFall()
	 */
	public void fall() throws IllegalArgumentException {
		if(!canFall())
			throw new IllegalArgumentException();
		double oldCoordinateY = getCoordinateY();
		while( this.getWorld().isPassable(getCoordinateX(), getCoordinateY(), getRadius()) && 
				!this.getWorld().isAdjacentToImpassableTerrain(getCoordinateX(), getCoordinateY(), getRadius()) && 
				 !this.isTerminated()){
			setYCoordinate(getCoordinateY()-0.01);
			if(this.getCoordinateY() < 0){
				this.terminate();
			}
		}
		int metersOfFall = (int)Math.floor(oldCoordinateY-getCoordinateY());
		setCurrentHitPoints(getCurrentHitPoints()-3*metersOfFall);
	}
	
	// TURN
	
	/**
	 * A method that changes the direction of this worm with a given value.
	 * 
	 * @param 		direction
	 * 				The new direction of this worm.
	 * @pre			The provided value has to be a valid direction.
	 * 				|isValidDirection(direction)
	 * @pre			The worm must have enough action points.
	 * 				|canTurn(direction)
	 * @post		The new direction is equal to the current direction plus the provided value.
	 * 				| new.getDirection() == this.getDirection() + direction
	 * @post		The action points needed for the turn must be subtracted from the current action points.
	 * 				| new.getCurrentActionPoints() == getCurrentActionPoints() - Math.abs((60/((2*Math.PI)/direction))) 
	 */
	public void turn(double direction) {
		assert isValidDirection(direction);
		assert canTurn(direction);
		setDirection(this.getDirection()+direction);
		setCurrentActionPoints((int)Math.ceil(this.getCurrentActionPoints() - Math.abs((60/((2*Math.PI)/direction)))));
	}
	
	/**
	 * A method to check whether this worm has enough action points to turn.
	 * 
	 * @return 		returns true when the used amount of action point, 
	 * 				subtracted from the current amount of action point is bigger than the minimum amount of action points.
	 * 				| result == Math.ceil(this.getCurrentActionPoints() - (direction / Math.abs((60/((2*Math.PI)/direction))))) 
	 * 				|			>= this.getMinimumActionPoints()
	 */
	public boolean canTurn(double direction) {
		return (Math.ceil(this.getCurrentActionPoints() - Math.abs(60/((2*Math.PI)/direction))) >= this.getMinimumActionPoints());
	}
	
	//JUMP 
	
	/**
	 * A method that returns the force invoked on this worm.
	 * 
	 * @return		The force invoked on this worm.
	 * 				| result == (5*getCurrentActionPoints())+(getMass()*getAccelerationOfEarth())
	 */
	public double getForce(){
		return ((5*getCurrentActionPoints())+(getMass()*getAccelerationOfEarth()));
	}
	
	/**
	 * A method that returns the acceleration of the earth.
	 */
	@Basic @Immutable
	public double getAccelerationOfEarth(){
		return 9.80665;
	}
	
	/**
	 * A method that returns the velocity of this worm.
	 * 
	 * @return		The velocity of this worm.
	 * 				| result == (getForce()/getMass())*0.5
	 */
	public double getVelocity(){
		return (((getForce()/getMass()))*0.5);
	}
	
	/**
	 *  A method to check whether the x-coordinate and y-coordinate 
	 *  are valid in the world of this worm.
	 *  
	 * @param	xCoordinate
	 * 			The x-coordinate to check.
	 * @param	yCoordinate
	 * 			The y-coordinate to check.
	 * @return	result is true when the coordinates are in the boundaries
	 * 			 of the world of this worm.
	 * 			| result == (xCoordinate >= 0) 
	 *			|      		&& (xCoordinate <= this.getWorld().getWidth()) 
	 *			|			&& (yCoordinate >= 0) 
	 *			|			&& (yCoordinate <= this.getWorld().getHeight())
	 */
	public boolean validWorldCoordinates(double xCoordinate, double yCoordinate){
		return (xCoordinate >= 0) 
				&& (xCoordinate <= this.getWorld().getWidth()) 
				&& (yCoordinate >= 0) 
				&& (yCoordinate <= this.getWorld().getHeight());
	}
	
	/**
	 * A method to check whether this worm can jump.
	 * 
	 * @return 		The method returns true if the current action points are bigger than
	 * 				the minimum amount of action points and if the worm is located on passable terrain.
	 * 				| result == (this.getCurrentActionPoints() > this.getMinimumActionPoints()) 
	 * 				| && ( this.getWorld().isPassable(getCoordinateX(), getCoordinateY(), getRadius())))
	 */
	public boolean canJump(){
		return ((this.getCurrentActionPoints() > this.getMinimumActionPoints()) 
				&& ( this.getWorld().isPassable(getCoordinateX(), getCoordinateY(), getRadius())));
	}
	
	/**
	 * A method to let this worm jump.
	 * 
	 * @param 	timeStep
	 * 			The timeStep An elementary time interval during which you may
	 * 			assume that the projectile will not completely move through a 
	 * 			piece of impassable terrain.
	 * @effect	The x-coordinate is set to the found location.
	 * 			| setXCoordinate(this.jumpStep(getJumpTime(timeStep))[0])
	 * @effect	The y-coordinate is set to the found location.
	 * 			| setYCoordinate(this.JumpStep(getJumpTime(timeStep))[1])
	 * @throws 	IllegalArgumentException
	 * 			An exception is thrown when this worm cannot jump.
	 * 			| ! this.canJump()
	 */
	public void jump(double timeStep) throws IllegalArgumentException{
		if(!this.canJump())
			throw new IllegalArgumentException();
		double[] inFlightCoordinates = this.jumpStep(getJumpTime(timeStep));
		this.setXCoordinate(inFlightCoordinates[0]);
		this.setYCoordinate(inFlightCoordinates[1]);
		this.setCurrentActionPoints(0);
	}
	
	/**
	 * A method to determine the time that this worm needs until it hits passable terrain
	 * adjacent to impassable terrain or leaves the world.
	 * 
	 * @param	timeStep
	 * 			timeStep An elementary time interval during which you may assume
	 *          that the worm will not completely move through a piece of impassable terrain.
	 * @return	The time needed for this worm until it hits passable terrain adjacent to impassable
	 * 			terrain or leaves the world.
	 * 			| double time = 0
	 * 			| while( (getWorld().isPassable(inFlightCoordinates[0], inFlightCoordinates[1], getRadius()))
	 *			|	    && (getWorld().isAdjacentToImpassableTerrain(inFlightCoordinates[0], inFlightCoordinates[1], getRadius()))
	 *			|		&&	validWorldCoordinates(inFlightCoordinates[0], inFlightCoordinates[1]) )
	 *			|		time = time + timeStep
	 *			|		inFlightCoordinates = this.jumpStep(time)
	 *			| while( (getWorld().isPassable(inFlightCoordinates[0], inFlightCoordinates[1], getRadius())) 
	 *			|		&& !(getWorld().isAdjacentToImpassableTerrain(inFlightCoordinates[0], inFlightCoordinates[1], getRadius()))
	 *			|		&&	validWorldCoordinates(inFlightCoordinates[0], inFlightCoordinates[1]) )
	 *			|		time = time + timeStep
	 *			|		inFlightCoordinates = this.jumpStep(time);
	 *			| return time
	 */
	public double getJumpTime(double timeStep) {
		double time = 0;
		double[] inFlightCoordinates = this.jumpStep(time);
		while( (this.getWorld().isPassable(inFlightCoordinates[0], inFlightCoordinates[1], getRadius()))
				&& (this.getWorld().isAdjacentToImpassableTerrain(inFlightCoordinates[0], inFlightCoordinates[1], getRadius()))
				&& 	validWorldCoordinates(inFlightCoordinates[0], inFlightCoordinates[1])){
			time = time + timeStep;
			inFlightCoordinates = this.jumpStep(time);
		}
		while( (this.getWorld().isPassable(inFlightCoordinates[0], inFlightCoordinates[1], getRadius())) 
				&& !(this.getWorld().isAdjacentToImpassableTerrain(inFlightCoordinates[0], inFlightCoordinates[1], getRadius()))
				&& 	validWorldCoordinates(inFlightCoordinates[0], inFlightCoordinates[1])){
			time = time + timeStep;
			inFlightCoordinates = this.jumpStep(time);
		}
		return time;
	}
	
	/**
	 * A method to compute the in-flight positions x-coordinate and 
	 * y-coordinate at any time after the worm is launched.
	 * 
	 * @param 	time
	 * 			The time after the worm is launched.
	 * @return	An array containing the new values for the X and Y coordinate at time.
	 *			|result == 
	 *			| new.getCoordinateX = this.getCoordinateX() + (((this.getVelocity()*Math.cos(this.getDirection()))*time))
	 *			| new.getCoordinateY = this.getCoordinateY() + ((this.getVelocity()*Math.sin(this.getDirection())*time) - ((this.getAccelerationOfEarth()*Math.pow(time, 2))/2))
	 *			| double[] result = {new.getCoordinateX, new.getCoordinateY}
	 * @throws 	IllegalArgumentException
	 * 			The method throws the exception when the worm cannot be launched.
	 * 			| ! this.canJump()
	 */
	public double[] jumpStep(double time) throws IllegalArgumentException{
		if(!this.canJump())
			throw new IllegalArgumentException();
		double x = this.getCoordinateX() + (((this.getVelocity()*Math.cos(this.getDirection()))*time));
		double y = this.getCoordinateY() + ((this.getVelocity()*Math.sin(this.getDirection())*time) - ((this.getAccelerationOfEarth()*Math.pow(time, 2))/2));
		double[] result = {x, y};
		return result;
	}
		
	
	// PROGRAM
	
	/** 
	 * A method to return the program of this worm.
	 */
	@Basic @Raw 
	public Program getProgram() {
		return this.program;
	}
		
	/**
	 * A method to check whether this worm can have the given program as its program.
	 * 
	 * @param	program
	 *          The program to check.
	 * @return	If this worm is not yet terminated, true if 
	 * 			the given program is effective or not effective.
	 *        	| if (! isTerminated())
	 *       	|   then result == (program != null) || (program == null) 
	 * @return	If this worm is terminated, true if and only if
	 *          the given program is not effective.
	 *        	| if (! this.isTerminated())
	 *       	|   then result == (program == null)
	 */
	@Raw
	public boolean canHaveAsProgram(Program program) {
		if (isTerminated())
			return (program == null);
		return (program != null || program == null);
	}
		
	/**
	 * A method to check whether this worm has a proper program.
	 * 
	 * @return	True if and only if this worm can have its program as its
	 * 			program, and if this worm is terminated and the program of
	 *          this worm has this worm as its worm or the program of this worm
	 *          is not effective.
	 *       	| result ==
	 *        	|   canHaveAsProgram(getProgram()) &&
	 *       	|   ( isTerminated() || getProgram() == null || getProgram().hasAsWorm(this))
	 */
	@Raw
	public boolean hasProperProgram() {
		return canHaveAsProgram(getProgram()) &&
				 ( isTerminated() || getProgram() == null || getProgram().getWorm() == this);
	}
			
	/** 
	 * A method to register the given program as the program of this worm.
	 * 
	 * @param	program
	 *          The program to be registered as the program of this worm.
	 * @post   	The program of this worm is the same as the given program.
	 *       	| new.getProgram() == program
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided program is not valid.
	 * 			| ! canHaveAsProgram(program)
	 */
	@Raw
	private void setProgram(Program program) throws IllegalArgumentException {
		if( !canHaveAsProgram(program) )
			throw new IllegalArgumentException("The provided program is not a valid program.");
		this.program = program;
	}
	
	/**
	 * A method to check whether this worm has an effective program.
	 * 
	 * @return	The result is true if this worm has an effective
	 * 			program.
	 * 			| if( getProgram() != null)
	 * 			|	result == true
	 */
	@Raw
	public boolean hasProgram(){
		if(getProgram() != null)
			return true;
		return false;
	}
			
	/**
	 * Variable referencing the program of this worm.
	 */
	private Program program = null;

	// LIFE
	
	/**
	 *  A method to check whether the given worm is alive.
	 *  
	 * @effect	If the result is false and only if it is false,
	 * 			This worm is terminated.
	 * 			| new.isTerminated()
	 * @return	The result is true if the worm his hit points are greater then zero.
	 * 			| result == getCurrentHitPoints() > 0
	 */
	@Raw
	public boolean isAlive() {
		if(getCurrentHitPoints() <= 0){
			this.terminate();
			return false;
		}
		return isTerminated();
	}
		
	// SHOOTING
	
	/**
	 * A method to check whether the propulsion yield is valid.
	 * 
	 * @param	propulsionYield
	 * 			The propulsion yield to check.
	 * @return	the result is true if the given propulsion yield is between
	 * 			0 and 100
	 * 			| result == (propulsionYield >= 0) && (propulsionYield <= 100)
	 */
	@Raw
	public boolean isValidPropulsionYield(int propulsionYield){
		return (propulsionYield >= 0) && (propulsionYield <= 100);
	}
	
	/**
	 * A method to shoot with this worm.
	 * 
	 * @param	propulsionYield
	 * 			The given propulsion yield.
	 * @pre		The propulsionYield must be valid.
	 * 			| isValidPropulsionYield(propulsionYield)
	 * @post	If the selected weapon is the Rifle, then
	 * 			we created a rifleBullet and decreased the current	
	 * 			action points of the worm with the shooting cost of
	 * 			the rifleBullet.	
	 * 			| if( this.getSelectedWeapon() instanceof Rifle )
	 * 			|  		then	double x = (this.getCoordinateX()+(Math.cos(this.getDirection())*this.getRadius())+0.1)
	 *			|			&&	double y = (this.getCoordinateY()+(Math.sin(this.getDirection())*this.getRadius())+0.1)
	 * 			|			&&	RifleBullet rifleBullet = new RifleBullet(x, y, this.getDirection(), this.getSelectedWeapon(), this.getWorld())
	 * 			|			&& setCurrentActionPoints(getCurrentActionPoints()-rifleBullet.getShootingCost())
	 * @post	If the selected weapon is the Bazooka, then
	 * 			We created a BazookaBullet and decreased the current
	 * 			action points of the worm with the shooting cost of
	 * 			the bazookaBullet.
	 * 			| if ( this.getSelectedWeapon() instanceof Bazooka )
	 *			|  		then	double x = (this.getCoordinateX()+(Math.cos(this.getDirection())*this.getRadius())+0.1)
	 *			|			&&	double y = (this.getCoordinateY()+(Math.sin(this.getDirection())*this.getRadius())+0.1)
	 *			|			&&	BazookaBullet bazookaBullet = new BazookaBullet(x, y, this.getDirection(), this.getSelectedWeapon(), this.getWorld(), propulsionYield)
	 *			|			&&	setCurrentActionPoints(getCurrentActionPoints()-bazookaBullet.getShootingCost())
	 */
	public void shoot(int propulsionYield){
		assert isValidPropulsionYield(propulsionYield);
		if (this.getSelectedWeapon() instanceof Rifle){
			double x = (this.getCoordinateX()+(Math.cos(this.getDirection())*this.getRadius())+0.1);
			double y = (this.getCoordinateY()+(Math.sin(this.getDirection())*this.getRadius())+0.1);
			RifleBullet rifleBullet = new RifleBullet(x, y, this.getDirection(), this.getSelectedWeapon(), this.getWorld());
			setCurrentActionPoints(getCurrentActionPoints()-rifleBullet.getShootingCost());
		}
		if (this.getSelectedWeapon() instanceof Bazooka){
			double x = (this.getCoordinateX()+(Math.cos(this.getDirection())*this.getRadius())+0.1);
			double y = (this.getCoordinateY()+(Math.sin(this.getDirection())*this.getRadius())+0.1);
			BazookaBullet bazookaBullet = new BazookaBullet(x, y, this.getDirection(), this.getSelectedWeapon(), this.getWorld(), propulsionYield);
			setCurrentActionPoints(getCurrentActionPoints()-bazookaBullet.getShootingCost());
		}
	}
	
	// TEAM
	
	/** 
	 * A method to return the team of this worm.
	 *    A null reference is returned if this worm has no team.
	 */
	@Basic @Raw
	public Team getTeam() {
		return this.team;
	}
	
	/**
	 * A method to check whether this worm can have the given team as his team.
	 *
	 * @param	team
	 *         	The team to check.
	 * @return 	True if the team is not effective.
	 *       	| if (team == null)
	 *       	|   then result == true
	 * @return	if the team is effective the team may not 
	 * 			contain this worm.
	 * 			|if (team != null)
	 * 			| 	then result == !(team.hasAsWorm(this))
	 */
	@Raw
	public boolean canHaveAsTeam(Team team) {
		if (team == null)
			return true;
		else
			return !(team.hasAsWorm(this));
	}
	
	/**
	 * A method to check whether this worm has a proper team.
	 *
	 * @return	True if and only if this worm can have its team
	 *         	as its team, and if the team is effective, and has this
	 *         	worm as one of its worms.
	 *       	| result ==
	 *       	|   canHaveAsTeam(getTeam()) &&
	 *       	|   ( (getTeam() == null) ||
	 *       	|     (getTeam().hasAsWorm(this))
	 */
	@Raw
	public boolean hasProperTeam() {
		return canHaveAsTeam(this.getTeam()) 
				&& ( (this.getTeam() == null) || (this.getTeam().hasAsWorm(this)) );
	}
	
	/** 
	 * A method to register the given team as the team of this worm.
	 * 
	 * @param	team
	 *         	The team to be registered as the team of this worm.
	 * @pre    	This team must be able to have the given worm as one
	 *          of its worms.
	 *       	| canHaveAsTeam(team)
	 * @post   	The team of this worm is the same as the given team.
	 *       	| new.getTeam() == team
	 */
	@Raw
	private void setTeam(Team team) {
		assert canHaveAsTeam(team);
		this.team = team;
	}
	
	/**
	 * Variable referencing the team of this worm.
	 */
	private Team team;
	
	// WEAPONS
	
	/**
	 * A method that returns a list of all the weapons of this worm.
	 */
	@Basic @Raw
	public ArrayList<Weapon> getWeapons() {
		return weaponObjects;
	}
	
	/**
	 * A method to check whether this worm can have the given weapon as one 
	 * of its weapons.
	 * 
	 * @param	weapon
	 *        	The weapon to check.
	 * @return	True if and only if the given weapon is effective
	 *        	and already references this worm, and this worm
	 *        	does not yet have a weapon of the class of the given weapon.
	 *       	| result ==
	 *       	|   (weapon != null) && (weapon.getWorm() == this)
				|		&& (!this.hasAsWeapon(weapon))
	 */
	@Raw
	public boolean canHaveAsWeapon(Weapon weapon) {
		return (weapon != null) && (weapon.getWorm() == this)
				&& (!this.hasAsWeapon(weapon));
	}
	
	/**
	 * A method to check whether this worm has a weapon of the class of the given weapon.
	 * 
	 * @param  	weapon
	 * 		   	The weapon to check.
	 * @return	The result is true when the worm has a weapon of the class 
	 * 			of the provided weapon, false otherwise.
	 *       	| for each I in 0..getWeapons().size()-1:
	 *      	|   if ( getWeapons().get(I).getClass() == weapon.getClass() ) 
	 *      	|     then result == true
	 *      	| result == false
	 */
	@Raw
	public boolean hasAsWeapon(Weapon weapon) {
		for(int i = 0; i < getWeapons().size(); i++)
			if(getWeapons().get(i).getClass() == weapon.getClass())
				return true;
		return false;
	}
	
	/**
	 * Add the given weapon to the list of weapons of this worm.
	 * 
	 * @param	weapon
	 *         	The weapon to be added.
	 * @post	The number of weapons of this worm is
	 *        	incremented by 1.
	 *      	| new.getWeapons().size() == this.getWeapons().size() + 1
	 * @post   	This worm has the given weapon as its very last weapon.
	 *       	| new.getWeapons().get(getWeapons().size()+1) == weapon
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided weapon is not a valid weapon.
	 * 			| ! canHaveAsWeapon(weapon)
	 */
	@Basic @Raw
	public void addWeapon(Weapon weapon) throws IllegalArgumentException{
		if(!canHaveAsWeapon(weapon))
			throw new IllegalArgumentException("The provided weapon is not a valid weapon.");
		weaponObjects.add(weapon);
	}
	
	/**
	 * A method to return the current selected weapon.
	 */
	@Basic @Raw
	public Weapon getSelectedWeapon(){
		return selectedWeapon;
	}
	
	/**
	 * A method to set the weapon of this worm.
	 * 
	 * @param	weapon
	 * 			The new weapon for this worm.
	 * @post	The new weapon for this worm is set to the given weapon.
	 * 			| new.getSelectedWeapon == weapon
	 */
	@Basic @Raw
	public void setSelectedWeapon(Weapon weapon){
		selectedWeapon = weapon;
	}
		
	/**
	 * A method to select the next weapon.
	 * 
	 * @post	If this worm has weapons and if the current selected weapon
	 * 			is the last weapon of this worm then is the new selected weapon
	 * 			the first weapon of this worm.
	 * 			| if( getWeapons().size() != 0 )
	 * 			|	then if( getWeapons().indexOf(getSelectedWeapon())+1 == getWeapons().size() )
	 * 			|			then setSelectedWeapon(getWeapons().get(0))
	 * @post	If this worm has weapons and if the current selected weapon
	 * 			is not the last weapon of this worm then the new selected weapon
	 * 			is the next weapon of this worm.
	 * 			| if( getWeapons().size() != 0 )
	 * 			| 	then if( getWeapons().indexOf(getSelectedWeapon())+1 != getWeapons().size() )
	 * 			|		then setSelectedWeapon(getWeapons().get( getWeapons().indexOf(getSelectedWeapon()) + 1 ))
	 */
	public void selectNextWeapon() {
		if(getWeapons().size() != 0){
			if(getWeapons().indexOf(getSelectedWeapon())+1 == getWeapons().size())
				setSelectedWeapon(getWeapons().get(0));
			else{
				setSelectedWeapon(getWeapons().get( getWeapons().indexOf(getSelectedWeapon()) + 1 ));
			}
		}
	}
	
	/**
	 * Variable referencing the current selected weapon of this worm.
	 */
	Weapon selectedWeapon;
	
	/**
	 * Variable referencing a list collecting all the weapons of this worm.
	 */
	ArrayList<Weapon> weaponObjects = new ArrayList<Weapon>();

}
