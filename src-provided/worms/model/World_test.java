package worms.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import worms.util.Util;

public class World_test {
	
	private static final double EPS = Util.DEFAULT_EPSILON;

	private IFacade facade;

	private Random random;

	private World world;

	// X X X X
	// . . . .
	// . . . .
	// X X X X
	private boolean[][] passableMap = new boolean[][] {
			{ false, false, false, false }, { true, true, true, true },
			{ true, true, true, true }, { false, false, false, false } };
	


	@Before
	public void setup() {
		facade = new Facade();
		random = new Random(7357);
		world = facade.createWorld(4.0, 4.0, passableMap, random);
	}
	
	// WIDTH & HEIGHT

	@Test
	public void getWidth_test() {
		assertEquals(4.0, world.getWidth(), EPS);
	}
	
	@Test
	public void getHeight_test() {
		assertEquals(4.0, world.getHeight(), EPS);
	}
	
	@Test
	public void isValidSizeOfWorld_LegalCase() {
		assertEquals(true, World.isValidSizeOfWorld(1, 1));
	}
	
	@Test
	public void isValidSizeOfWorld_IllegalCase() {
		assertEquals(false, World.isValidSizeOfWorld(-1, -1));
	}
	
	
	// TEAMS
	
	@Test
	public void addTeam() {
		Team team = new Team("HappyTesters", world);
		world.addTeam(team);

	}
	
	@Test
	public void hasAsTeam() {
		Team team = new Team("HappyTesters", world);
		assertTrue(world.hasAsTeam(team));
	}
	
	// FOOD
	
	@Test
	public void canHasAsFood_IllegalCase() {
		Food food = null;
		if (world.findRandomAdjacentLocation(Food.getRadius())) {
			food = new Food(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world);
		}
		
		assertFalse(world.canHaveAsGameObject(food));
	}
	
	@Test
	public void canHasAsFood_IllegalCase2() {
		Food food = null;
		assertFalse(world.canHaveAsGameObject(food));
	}
	
	@Test
	public void hasAsFood_LegalCase() {
		if (world.findRandomAdjacentLocation(Food.getRadius())) {
		}
		
		Food food = new Food(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world);
		
		assertTrue(world.hasAsGameObject(food)); 
	}
	

	@Test
	public void removeFood_LegalCase() {
		
		if (world.findRandomAdjacentLocation(Food.getRadius())) {
		}
		
		Food food = new Food(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world);
		food.terminate();
		assertFalse(world.hasAsGameObject(food)); 
	}
	
	
	
	// WORMS
	
	@Test
	public void hasAsWorm() {
		
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		assertTrue(world.hasAsGameObject(worm));
		
	}
	
	// LOCATION
	
	@Test 
	public void isAdjacentToImpassableTerrain_TestCase() {
		if(world.findRandomAdjacentLocation(0.50));
		assertTrue(world.isAdjacentToImpassableTerrain(world.getAdjacentLocationX(), world.getAdjacentLocationY(), 0.50));
	}
	
	@Test
	public void isImpassable_TestCase() {
		assertTrue(world.isImpassable(0, 0, 0.50));
	}
	
	@Test
	public void isPassable() {
		if(world.findRandomAdjacentLocation(0.50));
		assertTrue(world.isPassable(world.getAdjacentLocationX(), world.getAdjacentLocationY(), 0.50));
	}
	
	// GAME
	
	@Test
	public void setCurrentWorm_getCurrentWorm_TestCase() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		
		world.setCurrentWorm(worm);
		
		assertEquals(worm, world.getCurrentWorm());
	}
	

}
