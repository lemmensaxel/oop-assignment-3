package worms.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;

/**
 * A class representing the food of a world. 
 * 
 * @invar	Each food must have a valid x-coordinate.
 *       	| isValidCoordinate(getXCoordinate())
 * @invar	Each food must have a valid y-coordinate.
 *       	| isValidCoordinate(getYCoordinate())
 * @invar  	Each food must have a proper world.
 *       	| hasProperWorld()
 * 
 * @version 2.0
 * @author  Axel Lemmens & Maarten Rimaux
 */
public class Food extends GameObject {


	// CONSTRUCTOR
	
	/**
	 * @param	xCoordinate
	 * 			The provided value for the x-coordinate in meters.
	 * @param	yCoordinate
	 * 			The provided value for the y-coordinate in meters.
	 * @param	world
	 * 			The provided value for the world of this new food.
	 * @effect 	The x-coordinate for this new food is set to the provided value xCoordinate.
	 *      	| setXCoordinate(xCoordinate)
	 * @effect 	The y-coordinate for this new food is set to the provided value yCoordinate.
	 *      	| setYCoordinate(yCoordinate)
	 * @post	The new world of this new food is equal to the provided "world"
	 * 			| new.getWorld() == world
	 * @post   	The number of food for the given world is
	 *         	incremented by 1.
	 *       	| (new world).getFood().size() == world.getFood().size() + 1
	 * @post   	The given world has this new food as its very last food.
	 *      	| (new world).getFood().get(world.getFood().size()+1) == this
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided value is not a valid coordinate.
	 * 			| ! isValidCoordinate(xCoordinate)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided value is not a valid coordinate.
	 * 			| ! isValidCoordinate(yCoordinate)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when this new food cannot have the given world as
	 *         	its world.
	 * 			| ! canHaveAsWorld(world)
	 */
	
	public Food(double xCoordinate, double yCoordinate, World world)
			throws IllegalArgumentException {
		super(xCoordinate, yCoordinate, world);
	}
	
	// RADIUS
	
	/**
	 * A method to return the radius of this food item.
	 */
	@Basic @Immutable
	public static double getRadius(){
		return 0.20;
	}
	
	// EATEN
	
	/**
	 * A method to check whether this food is active or not.
	 * 
	 * @post	If the result is false and only if it is false, this
	 * 			food is terminated.
	 * 			| new.isTerminated() == true
	 * @return	The result is true if this food has not been eaten by a worm.
	 * 			|for each I in 0..this.getWorld().getWorms().size()-1
	 * 			|	if( (Math.sqrt( Math.pow(this.getCoordinateX() - this.getWorld().getWorms().get(i).getCoordinateX(), 2) +
	 *			|			    	Math.pow(this.getCoordinateY() - this.getWorld().getWorms().get(i).getCoordinateY(), 2) )) 
	 *			|		<= getRadius() + 0.2 )
	 *			|			then result == false
	 *			| result == true
	 */
	public boolean isActive(){
		for(int i = 0; i < this.getWorld().getWorms().size(); i++) {
			double distanceBetweenWormAndFood = Math.sqrt( Math.pow(this.getCoordinateX() - this.getWorld().getWorms().get(i).getCoordinateX(), 2) +
														   Math.pow(this.getCoordinateY() - this.getWorld().getWorms().get(i).getCoordinateY(), 2) );
			if( distanceBetweenWormAndFood < this.getWorld().getWorms().get(i).getRadius() ){
				this.getWorld().getWorms().get(i).setRadius(this.getWorld().getWorms().get(i).getRadius()+
														   (this.getWorld().getWorms().get(i).getRadius()/10));
				this.terminate();
				return false;
			}
		}
		return true;
	}

}
