package worms.model.statements;
import java.util.ArrayList;
import java.util.List;

import worms.model.Worm;
import worms.model.expressions.Expression;
import worms.model.expressions.Self;
import worms.model.types.Type;

public class Assignment extends Statement {
	
	public Assignment(String variableName, Expression<? extends Type> rhs){
		setVariableName(variableName);
		setRhs(rhs);
	}

	@Override
	public void execute(Worm worm) {
		if(worm.getProgram().getNumberOfExecutedStatements() < 1000){
			Self.setWorm(worm);
			worm.getProgram().getGlobals().put(getVariableName(), getRhs().getValue());
			worm.getProgram().setNumberOfExecutedStatements(worm.getProgram().getNumberOfExecutedStatements()+1);
		}
	}
	
	@Override
	public List<Statement> getChildren() {
		List<Statement> children = new ArrayList<Statement>();
		return children;
	}
	
	public String getVariableName(){
		return this.variableName;
	}
	
	public void setVariableName(String variableName){
		this.variableName = variableName;
	}
	
	public String variableName;
	
	public Expression<? extends Type> getRhs(){
		return this.rhs;
	}
	
	public void setRhs(Expression<? extends Type> rhs){
		this.rhs = rhs;
	}
	
	public Expression<? extends Type> rhs;

}
