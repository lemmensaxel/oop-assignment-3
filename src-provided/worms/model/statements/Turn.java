package worms.model.statements;

import java.util.ArrayList;
import java.util.List;

import worms.gui.game.IActionHandler;
import worms.model.Worm;
import worms.model.expressions.Expression;
import worms.model.expressions.Self;
import worms.model.types.DoubleType;


public class Turn extends Action {

	public Turn(Expression<DoubleType> angle) {
		this.angle = angle;
	}

	@Override
	public void execute(Worm worm) {
		Self.setWorm(worm);
		if (worm.getProgram().getNumberOfExecutedStatements() < 1000 && worm.canTurn(getAngle().getValue().getValue())) {
			worm.getProgram().setNumberOfExecutedStatements(worm.getProgram().getNumberOfExecutedStatements()+1);
			IActionHandler handler = worm.getProgram().getHandler();
			handler.turn(worm, getAngle().getValue().getValue());
		}
	}
	
	@Override
	public List<Statement> getChildren() {
		List<Statement> children = new ArrayList<Statement>();
		return children;
	}

	public Expression<DoubleType> getAngle(){
		return this.angle;
	}
	
	public void  setAngle(Expression<DoubleType> angle) {
		this.angle = angle;
	}
	
	private Expression<DoubleType> angle;
}
