package worms.model.statements;


import java.util.List;

import worms.model.Worm;


public abstract class Statement {
	
	public abstract void execute(Worm worm);
	
	public abstract List<Statement> getChildren();
	
		
}
