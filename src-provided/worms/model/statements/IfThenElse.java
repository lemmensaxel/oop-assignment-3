package worms.model.statements;


import java.util.ArrayList;
import java.util.List;

import worms.model.Worm;
import worms.model.expressions.Expression;
import worms.model.expressions.Self;
import worms.model.types.BoolType;

public class IfThenElse extends Statement {
	
	public IfThenElse(Expression<BoolType> condition, Statement then, Statement otherwise){
		this.condition = condition;
		this.then = then;
		this.otherwise = otherwise;
	}

	@Override
	public void execute(Worm worm) {
		Self.setWorm(worm);
		if(worm.getProgram().getNumberOfExecutedStatements() < 1000){
			worm.getProgram().setNumberOfExecutedStatements(worm.getProgram().getNumberOfExecutedStatements()+1);
			if((condition.getValue()).getState()){
				then.execute(worm);
			} else {
				otherwise.execute(worm);
			}	
		}
	}
	
	@Override
	public List<Statement> getChildren() {
		List<Statement> children = new ArrayList<Statement>();
		children.add(getThen());
		children.add(getOtherwise());
		return children;
	}
	
	public Expression<BoolType> getCondition(){
		return this.condition;
	}
	
	public void setCondition(Expression<BoolType> condition){
		this.condition = condition;
	}
	
	private Expression<BoolType> condition;
	
	public Statement getThen(){
		return this.then;
	}
	
	public void setThen(Statement then){
		this.then = then;
	}
	
	private Statement then;
	
	public Statement getOtherwise(){
		return this.otherwise;
	}
	
	public void setOtherwise(Statement otherwise){
		this.otherwise = otherwise;
	}
	
	private Statement otherwise;

}
