package worms.model.statements;

import java.util.ArrayList;
import java.util.List;

import worms.model.Food;
import worms.model.Worm;
import worms.model.expressions.Self;
import worms.model.programs.ProgramFactory.ForeachType;
import worms.model.types.EntityType;

public class ForEachLoop extends Statement {
	
	public ForEachLoop(ForeachType type, String VariableName, Statement body){
		setType(type);
		setVariableName(variableName);
		setBody(body);
	}

	@Override
	public void execute(Worm worm) {
		Self.setWorm(worm);
		if(worm.getProgram().getNumberOfExecutedStatements() < 1000){
			if(type == ForeachType.WORM){
				for( Worm w : worm.getWorld().getWorms()){
					EntityType wormmen = new EntityType(w);
					worm.getProgram().getGlobals().put(getVariableName(), wormmen);
					getBody().execute(worm);
				}
			}
			if(type == ForeachType.FOOD){
				for( Food f : worm.getWorld().getFood()){
					EntityType food = new EntityType(f);
					worm.getProgram().getGlobals().put(getVariableName(), food);
					getBody().execute(worm);
				}
			}
			if(type == ForeachType.ANY){
				for( Worm w : worm.getWorld().getWorms()){
					EntityType wormmen = new EntityType(w);
					worm.getProgram().getGlobals().put(getVariableName(), wormmen);
					getBody().execute(worm);
				}
				for( Food f : worm.getWorld().getFood()){
					EntityType food = new EntityType(f);
					worm.getProgram().getGlobals().put(getVariableName(), food);
					getBody().execute(worm);
				}
				
			}
		}
	}
	
	@Override
	public List<Statement> getChildren() {
		List<Statement> children = new ArrayList<Statement>();
		children.add(getBody());
		return children;
	}
	
	public ForeachType getType(){
		return this.type;
	}
	
	public void setType(ForeachType type){
		this.type= type;
	}
	
	private ForeachType type;
	
	public String getVariableName(){
		return this.variableName;
	}
	
	public void setVariableName(String variableName){
		this.variableName = variableName;
	}
	
	public String variableName;
	
	public Statement getBody(){
		return this.body;
	}
	
	public void setBody(Statement body){
		this.body = body;
	}
	
	private Statement body;

}
