package worms.model.statements;

import java.util.ArrayList;
import java.util.List;

import worms.model.Worm;
import worms.model.expressions.Self;

public class Sequence extends Statement {
	
	public Sequence(List<Statement> statements){
		this.setStatements(statements);
	}

	@Override
	public void execute(Worm worm) {
		Self.setWorm(worm);
		if (worm.getProgram().getNumberOfExecutedStatements() < 1000) {
			worm.getProgram().setNumberOfExecutedStatements(worm.getProgram().getNumberOfExecutedStatements()+1);
			for(Statement s : getStatements()){
				s.execute(worm);
			}
		}
	}
	
	@Override
	public List<Statement> getChildren() {
		List<Statement> children = new ArrayList<Statement>();
		children.addAll(getStatements());
		return children;
	}

	public List<Statement> getStatements(){
		return this.statements;
	}
	
	public void setStatements(List<Statement> statements){
		this.statements = statements;
	}
	
	private List<Statement> statements;
}
