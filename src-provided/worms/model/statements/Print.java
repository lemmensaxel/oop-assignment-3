package worms.model.statements;

import java.util.ArrayList;
import java.util.List;

import worms.gui.game.IActionHandler;
import worms.model.Worm;
import worms.model.expressions.Expression;
import worms.model.expressions.Self;
import worms.model.types.Type;


public class Print extends Statement {
	
	public Print(Expression<? extends Type> expression){
		this.setExpression(expression);
	}

	@Override
	public void execute(Worm worm) {
		Self.setWorm(worm);
		if (worm.getProgram().getNumberOfExecutedStatements() < 1000) {
			worm.getProgram().setNumberOfExecutedStatements(worm.getProgram().getNumberOfExecutedStatements()+1);
			IActionHandler handler = worm.getProgram().getHandler();
			handler.print(getExpression().getValue().toString());;
		}
	}
	
	@Override
	public List<Statement> getChildren() {
		List<Statement> children = new ArrayList<Statement>();
		return children;
	}
	
	public Expression<? extends Type> getExpression(){
		return this.expression;
	}
	
	public void setExpression(Expression<? extends Type> expression){
		this.expression = expression;
	}
	
	private Expression<? extends Type> expression;

}
