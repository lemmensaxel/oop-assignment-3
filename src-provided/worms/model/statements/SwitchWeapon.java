package worms.model.statements;

import java.util.ArrayList;
import java.util.List;

import worms.gui.game.IActionHandler;
import worms.model.Worm;
import worms.model.expressions.Self;


public class SwitchWeapon extends Action {

	public SwitchWeapon() {
	}

	@Override
	public void execute(Worm worm) {
		Self.setWorm(worm);
		if (worm.getProgram().getNumberOfExecutedStatements() < 1000) {
			worm.getProgram().setNumberOfExecutedStatements(worm.getProgram().getNumberOfExecutedStatements()+1);
			IActionHandler handler = worm.getProgram().getHandler();
			handler.toggleWeapon(worm);
		}
	}
	
	@Override
	public List<Statement> getChildren() {
		List<Statement> children = new ArrayList<Statement>();
		return children;
	}

}
