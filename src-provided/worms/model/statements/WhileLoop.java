package worms.model.statements;

import java.util.ArrayList;
import java.util.List;

import worms.model.Worm;
import worms.model.expressions.Expression;
import worms.model.expressions.Self;
import worms.model.types.BoolType;

public class WhileLoop extends Statement {
	
	public WhileLoop(Expression<BoolType> condition, Statement body){
		this.setCondition(condition);
		this.setBody(body);
	}

	@Override
	public void execute(Worm worm) {
		Self.setWorm(worm);
		if(worm.getProgram().getNumberOfExecutedStatements() < 1000){
			worm.getProgram().setNumberOfExecutedStatements(worm.getProgram().getNumberOfExecutedStatements()+1);
			while(condition.getValue().getState()){
				body.execute(worm);
				if(worm.getProgram().getNumberOfExecutedStatements() >= 1000)
					break;
			}
		}
	}
	
	@Override
	public List<Statement> getChildren() {
		List<Statement> children = new ArrayList<Statement>();
		children.add(getBody());
		return children;
	}
	
	public Expression<BoolType> getCondition(){
		return this.condition;
	}
	
	public void setCondition(Expression<BoolType> condition){
		this.condition = condition;
	}
	
	private Expression<BoolType> condition;
	
	public Statement getBody(){
		return this.body;
	}
	
	public void setBody(Statement body){
		this.body = body;
	}
	
	private Statement body;
	

}
