package worms.model.statements;

import java.util.ArrayList;
import java.util.List;

import worms.model.Worm;
import worms.model.expressions.Self;


public class Hold extends Action {

	public Hold() {}

	@Override
	public void execute(Worm worm) {
		Self.setWorm(worm);
		if (worm.getProgram().getNumberOfExecutedStatements() < 1000) {
			worm.getProgram().setNumberOfExecutedStatements(worm.getProgram().getNumberOfExecutedStatements()+1);
		}
	}
	
	@Override
	public List<Statement> getChildren() {
		List<Statement> children = new ArrayList<Statement>();
		return children;
	}

}
