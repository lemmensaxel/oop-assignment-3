package worms.model.statements;

import java.util.ArrayList;
import java.util.List;

import worms.gui.game.IActionHandler;
import worms.model.Worm;
import worms.model.expressions.Expression;
import worms.model.expressions.Self;
import worms.model.types.DoubleType;

public class Shoot extends Action {

	public Shoot(Expression<DoubleType> yield) {
		this.setYield(yield);
	}

	@Override
	public void execute(Worm worm) {
		Self.setWorm(worm);
		if (worm.getProgram().getNumberOfExecutedStatements() < 1000 && worm.getCurrentActionPoints() > 80) {
			worm.getProgram().setNumberOfExecutedStatements(worm.getProgram().getNumberOfExecutedStatements()+1);
			IActionHandler handler = worm.getProgram().getHandler();
			handler.fire(worm, (int)getYield().getValue().getValue());
		}
	}
	
	@Override
	public List<Statement> getChildren() {
		List<Statement> children = new ArrayList<Statement>();
		return children;
	}
	
	public Expression<DoubleType> getYield(){
		return this.yield;
	}
	
	public void  setYield(Expression<DoubleType> yield) {
		this.yield = yield;
	}
	
	private Expression<DoubleType> yield;

}
