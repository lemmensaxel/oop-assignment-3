package worms.model.expressions.basicexpressions;

import worms.model.expressions.Expression;


public abstract class BasicExpression<T> extends Expression<T> {

	public BasicExpression(T value) {
		this.value = value;
	}
	
	public T getValue() {
		return value;
	}
	
	private final T value;
}
