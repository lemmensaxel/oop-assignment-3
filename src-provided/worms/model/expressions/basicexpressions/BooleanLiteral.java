package worms.model.expressions.basicexpressions;

import worms.model.types.*;

public class BooleanLiteral extends BasicExpression<BoolType> {

	public BooleanLiteral(BoolType value) {
		super(value);
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof BooleanLiteral)
				&& ((getValue()).getState() == (((BooleanLiteral) other).getValue()).getState());
	}
	
}
