package worms.model.expressions.basicexpressions;

import worms.model.types.*;

public class ObjectLiteral extends BasicExpression<EntityType>{

	public ObjectLiteral(EntityType value) {
		super(value);
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof ObjectLiteral)
				&& (getValue().getObject() == ((ObjectLiteral) other).getValue().getObject());
	}
}
