package worms.model.expressions.basicexpressions;

import worms.model.types.*;

public class DoubleLiteralConstant extends BasicExpression<DoubleType> {

	public DoubleLiteralConstant(DoubleType value) {
		super(value);
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof DoubleLiteralConstant)
				&& ((getValue()).getValue() == (((DoubleLiteralConstant) other).getValue()).getValue());
	}

}
