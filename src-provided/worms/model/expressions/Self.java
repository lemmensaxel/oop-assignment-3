package worms.model.expressions;

import worms.model.Worm;
import worms.model.expressions.basicexpressions.ObjectLiteral;
import worms.model.types.EntityType;

public class Self extends Expression<EntityType> {
	
	public Self() {
	}

	@Override
	public EntityType getValue() {
		return new EntityType(worm);
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof ObjectLiteral)
				&& (getValue() == ((ObjectLiteral) other).getValue().getObject());
	}
	
	public static void setWorm(Worm worm){
		Self.worm = worm;
	}

	public static Worm worm;
}
