package worms.model.expressions;

import worms.model.types.Type;


public class VariableAcces extends Expression<Type> {
	
	public VariableAcces(String name){
		this.setName(name);
	}

	public VariableAcces(Type variable, String name){
		this.setVariable(variable);
		this.setName(name);
		
	}
	@Override
	public Type getValue() {
		return Self.worm.getProgram().getGlobals().get(getName());
	}
	
	public Type getVariable(){
		return this.variable;
	}
	
	public void setVariable(Type variable){
		this.variable = variable;
	}

	private Type variable;
	
	public String getName(){
		return this.name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	private String name;
	
	@Override
	public boolean equals(Object other) {
		return (other instanceof VariableAcces)
				&& (getValue() == ((VariableAcces) other).getValue());
	}
	

}
