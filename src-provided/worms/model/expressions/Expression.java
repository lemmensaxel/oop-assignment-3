package worms.model.expressions;

public abstract class Expression<T>{

	public abstract T getValue();

	@Override
	public abstract boolean equals(Object other);

}