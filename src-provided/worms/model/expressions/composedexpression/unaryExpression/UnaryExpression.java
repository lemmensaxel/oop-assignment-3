package worms.model.expressions.composedexpression.unaryExpression;

import worms.model.expressions.Expression;
import worms.model.expressions.composedexpression.ComposedExpression;


public abstract class UnaryExpression<T> extends ComposedExpression<T>{
	
	protected UnaryExpression(Expression<T> operand) {
		setOperandAt(1, operand);
	}

	@Override
	public final int getNbOperands() {
		return 1;
	}

	@Override
	public final boolean canHaveAsNbOperands(int number) {
		return number == 1;
	}

	@Override
	public final Expression<T> getOperandAt(int index)
			throws IndexOutOfBoundsException {
		if (index != 1)
			throw new IndexOutOfBoundsException();
		return getOperand();
	}

	public Expression<T> getOperand() {
		return operand;
	}

	@Override
	protected void setOperandAt(int index, Expression<T> operand) {
		this.operand = operand;
	}

	private Expression<T> operand;

}
