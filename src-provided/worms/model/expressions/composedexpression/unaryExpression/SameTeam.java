package worms.model.expressions.composedexpression.unaryExpression;

import worms.model.Worm;
import worms.model.expressions.Expression;
import worms.model.expressions.Self;
import worms.model.types.BoolType;
import worms.model.types.EntityType;
import worms.model.types.Type;

public class SameTeam extends UnaryExpression<Type> {

	public SameTeam(Expression<Type> operand) {
		super(operand);
	}

	@Override
	public BoolType getValue() {
		if(getOperand().getValue() instanceof EntityType)
			if(((EntityType)getOperand().getValue()).getObject() instanceof Worm)
				return new BoolType(((Worm)((EntityType)getOperand().getValue()).getObject()).getTeam().equals(Self.worm.getTeam()));
		return new BoolType();
	}
	
}
