package worms.model.expressions.composedexpression.unaryExpression;

import worms.model.expressions.Expression;
import worms.model.types.DoubleType;

public class SquareRoot extends UnaryExpression<DoubleType>{
	
	public SquareRoot(Expression<DoubleType> operand) {
		super(operand);
	}

	@Override
	public DoubleType getValue() {
		return new DoubleType(Math.sqrt((getOperand().getValue()).getValue()));
	}

}
