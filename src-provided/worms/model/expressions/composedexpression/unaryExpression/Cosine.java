package worms.model.expressions.composedexpression.unaryExpression;

import worms.model.expressions.Expression;
import worms.model.types.DoubleType;

public class Cosine extends UnaryExpression<DoubleType> {
	
	public Cosine(Expression<DoubleType> operand) {
		super(operand);
	}

	@Override
	public DoubleType getValue() {
		return new DoubleType(Math.cos((getOperand().getValue()).getValue()));
	}

}
