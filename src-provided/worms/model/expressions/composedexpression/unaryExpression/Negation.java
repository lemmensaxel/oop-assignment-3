package worms.model.expressions.composedexpression.unaryExpression;

import worms.model.expressions.Expression;
import worms.model.types.BoolType;


public class Negation extends UnaryExpression<BoolType> {
	
	public Negation(Expression<BoolType> operand) {
		super(operand);
	}

	@Override
	public BoolType getValue() {
		return new BoolType(!(getOperand().getValue()).getState());
	}

}
