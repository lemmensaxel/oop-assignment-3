package worms.model.expressions.composedexpression.unaryExpression;

import worms.model.Food;
import worms.model.expressions.Expression;
import worms.model.types.*;

public class IsFood extends UnaryExpression<Type> {

	public IsFood(Expression<Type> operand) {
		super(operand);
	}
	
	@Override
	public BoolType getValue() {
		if(getOperand().getValue() instanceof EntityType)
			if(((EntityType)getOperand().getValue()).getObject() instanceof Food)
				return new BoolType(true);
		return new BoolType();
	}

}
