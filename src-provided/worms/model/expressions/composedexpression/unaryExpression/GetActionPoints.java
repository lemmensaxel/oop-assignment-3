package worms.model.expressions.composedexpression.unaryExpression;

import worms.model.Worm;
import worms.model.expressions.Expression;
import worms.model.types.*;

public class GetActionPoints extends UnaryExpression<Type> {
	
	public GetActionPoints(Expression<Type> operand) {
		super(operand);
	}

	@Override
	public DoubleType getValue() {
		if(getOperand().getValue() instanceof EntityType)
			if(((EntityType)getOperand().getValue()).getObject() instanceof Worm)
				return new DoubleType(((Worm)((EntityType)getOperand().getValue()).getObject()).getCurrentActionPoints());
	return new DoubleType();
	}

}
