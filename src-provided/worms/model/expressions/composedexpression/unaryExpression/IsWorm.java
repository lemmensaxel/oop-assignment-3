package worms.model.expressions.composedexpression.unaryExpression;

import worms.model.Worm;
import worms.model.expressions.Expression;
import worms.model.types.*;

public class IsWorm extends UnaryExpression<Type> {
	
	public IsWorm(Expression<Type> operand) {
		super(operand);
	}
	
	@Override
	public BoolType getValue() {
		if(getOperand().getValue() instanceof EntityType)
			if(((EntityType)getOperand().getValue()).getObject() instanceof Worm)
				return new BoolType(true);
		return new BoolType();
	}

}
