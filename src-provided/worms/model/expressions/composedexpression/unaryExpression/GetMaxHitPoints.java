package worms.model.expressions.composedexpression.unaryExpression;

import worms.model.Worm;
import worms.model.expressions.Expression;
import worms.model.types.*;

public class GetMaxHitPoints extends UnaryExpression<Type> {
	
	public GetMaxHitPoints(Expression<Type> operand) {
		super(operand);
	}

	@Override
	public DoubleType getValue() {
		if(getOperand().getValue() instanceof EntityType)
			if(((EntityType)getOperand().getValue()).getObject() instanceof Worm)
				return new DoubleType(((Worm)((EntityType)getOperand().getValue()).getObject()).getMaximumHitPoints());
		return new DoubleType();
	}

}
