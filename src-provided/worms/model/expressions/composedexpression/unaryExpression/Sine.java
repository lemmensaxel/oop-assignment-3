package worms.model.expressions.composedexpression.unaryExpression;

import worms.model.expressions.Expression;
import worms.model.types.DoubleType;

public class Sine extends UnaryExpression<DoubleType> {
	
	public Sine(Expression<DoubleType> operand) {
		super(operand);
	}

	@Override
	public DoubleType getValue() {
		return new DoubleType(Math.sin((getOperand().getValue()).getValue()));
	}

}
