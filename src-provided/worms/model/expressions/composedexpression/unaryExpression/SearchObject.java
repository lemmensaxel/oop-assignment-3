package worms.model.expressions.composedexpression.unaryExpression;

import worms.model.Worm;
import worms.model.expressions.Expression;
import worms.model.expressions.Self;
import worms.model.types.DoubleType;
import worms.model.types.EntityType;
import worms.model.types.Type;

public class SearchObject extends UnaryExpression<Type> {
	
	public SearchObject(Expression<Type> operand) {
		super(operand);
	}

	@Override
	public EntityType getValue() {
		
		double direction = Self.worm.getDirection() + ((DoubleType)getOperand().getValue()).getValue();
		double xCoordinate = Self.worm.getCoordinateX();
		double yCoordinate = Self.worm.getCoordinateY();
		double distance = Self.worm.getRadius()/10;
		
		while(xCoordinate >= 0 && yCoordinate >= 0 && xCoordinate < Self.worm.getWorld().getWidth() && yCoordinate < Self.worm.getWorld().getHeight()){
			xCoordinate = xCoordinate + (Math.cos(direction)*distance);
			yCoordinate = xCoordinate + (Math.sin(direction)*distance);
			if(wormCollisionDetection(xCoordinate, yCoordinate)){
				return new EntityType(getFoundWorm());
			}	
			distance += Self.worm.getRadius()/10;
		}
		
		return new EntityType();
	}
	
	public boolean wormCollisionDetection(double xCoordinate, double yCoordinate) {
		for(Worm w : Self.worm.getWorld().getWorms())
			if(w != Self.worm){
				double distanceBetweenWorms = Math.sqrt( Math.pow(xCoordinate - w.getCoordinateX(), 2) + Math.pow(yCoordinate - w.getCoordinateY(), 2));
				if(distanceBetweenWorms <= w.getRadius() ) {
					setFoundWorm(w);
					return true;
				}
			}
		return false;
	}
	
	public Worm getFoundWorm() {
		return foundWorm;
	}

	public void setFoundWorm(Worm foundedWorm) {
		this.foundWorm = foundedWorm;
	}

	private Worm foundWorm;
}
