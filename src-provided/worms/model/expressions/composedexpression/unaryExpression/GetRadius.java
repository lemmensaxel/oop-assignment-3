package worms.model.expressions.composedexpression.unaryExpression;

import worms.model.Food;
import worms.model.Worm;
import worms.model.expressions.Expression;
import worms.model.types.*;

public class GetRadius extends UnaryExpression<Type> {
	
	public GetRadius(Expression<Type> operand) {
		super(operand);
	}

	@Override
	public DoubleType getValue() {
		if(getOperand().getValue() instanceof EntityType)
			if(((EntityType)getOperand().getValue()).getObject() instanceof Worm) {
				return new DoubleType(((Worm)((EntityType)getOperand().getValue()).getObject()).getRadius());
			} else if(((EntityType)getOperand().getValue()).getObject() instanceof Food){
				return new DoubleType(Food.getRadius());
			}
		return new DoubleType();
	}

}
