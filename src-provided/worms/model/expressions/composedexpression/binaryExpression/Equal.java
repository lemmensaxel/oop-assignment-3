package worms.model.expressions.composedexpression.binaryExpression;

import worms.model.expressions.Expression;
import worms.model.types.BoolType;
import worms.model.types.Type;

public class Equal extends BinaryExpression<Type> {
	
	public Equal(Expression<Type> left, Expression<Type> right) {
		super(left, right);
	}

	@Override
	public BoolType getValue() {
		return new  BoolType((getLeftOperand()).equals(getRightOperand()));
	}

}
