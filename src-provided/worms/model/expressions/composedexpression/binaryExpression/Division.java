package worms.model.expressions.composedexpression.binaryExpression;

import worms.model.expressions.Expression;
import worms.model.types.DoubleType;

public class Division extends BinaryExpression<DoubleType> {
	
	public Division(Expression<DoubleType> left, Expression<DoubleType> right) {
		super(left, right);
	}

	@Override
	public DoubleType getValue() {
		if( (getRightOperand().getValue()).getValue() == 0 )
			return new DoubleType();
		return new DoubleType((getLeftOperand().getValue()).getValue() / (getRightOperand().getValue()).getValue());
	}

}
