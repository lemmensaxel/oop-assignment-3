package worms.model.expressions.composedexpression.binaryExpression;

import worms.model.expressions.Expression;
import worms.model.expressions.composedexpression.ComposedExpression;


public abstract class BinaryExpression<T> extends ComposedExpression<T> {
	
	protected BinaryExpression(Expression<T> left, Expression<T> right) {
		setOperandAt(1, left);
		setOperandAt(2, right);
	}

	@Override
	public final int getNbOperands() {
		return 2;
	}

	@Override
	public final boolean canHaveAsNbOperands(int number) {
		return number == 2;
	}

	@Override
	public final Expression<T> getOperandAt(int index)
			throws IndexOutOfBoundsException {
		if ((index != 1) && (index != 2))
			throw new IndexOutOfBoundsException();
		if (index == 1)
			return getLeftOperand();
		else
			return getRightOperand();
	}

	@Override
	protected void setOperandAt(int index, Expression<T> operand) {
		if (index == 1)
			this.leftOperand = operand;
		else
			this.rightOperand = operand;
	}

	public Expression<T> getLeftOperand() {
		return leftOperand;
	}

	private Expression<T> leftOperand;

	public Expression<T> getRightOperand() {
		return rightOperand;
	}

	private Expression<T> rightOperand;

}
