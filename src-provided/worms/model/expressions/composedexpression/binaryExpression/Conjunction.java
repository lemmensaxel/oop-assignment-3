package worms.model.expressions.composedexpression.binaryExpression;

import worms.model.expressions.Expression;
import worms.model.types.BoolType;

public class Conjunction extends BinaryExpression<BoolType>{
	
	public Conjunction(Expression<BoolType> left, Expression<BoolType> right) {
		super(left, right);
	}

	@Override
	public BoolType getValue() {
		return new BoolType((getLeftOperand().getValue()).getState() && (getRightOperand().getValue()).getState());
	}

}
