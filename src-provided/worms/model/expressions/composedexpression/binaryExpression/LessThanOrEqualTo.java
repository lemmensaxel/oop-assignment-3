package worms.model.expressions.composedexpression.binaryExpression;

import worms.model.expressions.Expression;
import worms.model.types.*;

public class LessThanOrEqualTo extends BinaryExpression<Type> {
	
	public LessThanOrEqualTo(Expression<Type> left, Expression<Type> right) {
		super(left, right);
	}

	@Override
	public BoolType getValue() {
		if(getLeftOperand().getValue() instanceof DoubleType && getRightOperand().getValue() instanceof DoubleType)
			return new BoolType(((DoubleType)getLeftOperand().getValue()).getValue() <= ((DoubleType)getRightOperand().getValue()).getValue());
		return new BoolType();
	}

}
