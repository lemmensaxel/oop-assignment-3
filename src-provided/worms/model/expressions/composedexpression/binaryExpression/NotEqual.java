package worms.model.expressions.composedexpression.binaryExpression;

import worms.model.expressions.Expression;
import worms.model.types.*;

public class NotEqual extends BinaryExpression<Type> {
	
	public NotEqual(Expression<Type> left, Expression<Type> right) {
		super(left, right);
	}

	@Override
	public BoolType getValue() {
		return new  BoolType(!((getLeftOperand()).equals(getRightOperand())));
	}

}
