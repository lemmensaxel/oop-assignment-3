package worms.model.expressions.composedexpression.binaryExpression;

import worms.model.expressions.Expression;
import worms.model.types.DoubleType;

public class Subtraction extends BinaryExpression<DoubleType> {
	
	public Subtraction(Expression<DoubleType> left, Expression<DoubleType> right) {
		super(left, right);
	}

	@Override
	public DoubleType getValue() {
		return new DoubleType((getLeftOperand().getValue()).getValue() - (getRightOperand().getValue()).getValue());
	}

}
