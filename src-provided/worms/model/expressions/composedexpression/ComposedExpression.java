package worms.model.expressions.composedexpression;

import worms.model.expressions.Expression;


public abstract class ComposedExpression<T> extends Expression<T> {
	
		@Override
		public boolean equals(Object other) {
			if ((other == null) || (getClass() != other.getClass()))
				return false;
			@SuppressWarnings("unchecked")
			ComposedExpression<T> otherExpr = (ComposedExpression<T>) other;
			if (getNbOperands() != otherExpr.getNbOperands())
				return false;
			for (int pos = 1; pos <= getNbOperands(); pos++)
				if (!getOperandAt(pos).equals(otherExpr.getOperandAt(pos)))
					return false;
			return true;
		}

		public abstract int getNbOperands();

		public boolean canHaveAsNbOperands(int nbOperands) {
			return nbOperands > 0;
		}

		public abstract Expression<T> getOperandAt(int index)
				throws IndexOutOfBoundsException;

		public boolean canHaveAsOperand(Expression<T> expression) {
			return (expression != null);
		}

		protected abstract void setOperandAt(int index, Expression<T> operand);

}
