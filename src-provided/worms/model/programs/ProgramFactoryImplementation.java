package worms.model.programs;

import java.util.List;

import worms.model.expressions.Expression;
import worms.model.expressions.Self;
import worms.model.expressions.VariableAcces;
import worms.model.expressions.basicexpressions.BooleanLiteral;
import worms.model.expressions.basicexpressions.DoubleLiteralConstant;
import worms.model.expressions.basicexpressions.ObjectLiteral;
import worms.model.expressions.composedexpression.binaryExpression.Addition;
import worms.model.expressions.composedexpression.binaryExpression.Conjunction;
import worms.model.expressions.composedexpression.binaryExpression.Disjunction;
import worms.model.expressions.composedexpression.binaryExpression.Division;
import worms.model.expressions.composedexpression.binaryExpression.Equal;
import worms.model.expressions.composedexpression.binaryExpression.GreatherThan;
import worms.model.expressions.composedexpression.binaryExpression.GreatherThanOrEqualTo;
import worms.model.expressions.composedexpression.binaryExpression.LessThan;
import worms.model.expressions.composedexpression.binaryExpression.LessThanOrEqualTo;
import worms.model.expressions.composedexpression.binaryExpression.Multiplication;
import worms.model.expressions.composedexpression.binaryExpression.NotEqual;
import worms.model.expressions.composedexpression.binaryExpression.Subtraction;
import worms.model.expressions.composedexpression.unaryExpression.Cosine;
import worms.model.expressions.composedexpression.unaryExpression.GetActionPoints;
import worms.model.expressions.composedexpression.unaryExpression.GetDirection;
import worms.model.expressions.composedexpression.unaryExpression.GetHitPoints;
import worms.model.expressions.composedexpression.unaryExpression.GetMaxActionPoints;
import worms.model.expressions.composedexpression.unaryExpression.GetMaxHitPoints;
import worms.model.expressions.composedexpression.unaryExpression.GetRadius;
import worms.model.expressions.composedexpression.unaryExpression.GetX;
import worms.model.expressions.composedexpression.unaryExpression.GetY;
import worms.model.expressions.composedexpression.unaryExpression.IsFood;
import worms.model.expressions.composedexpression.unaryExpression.IsWorm;
import worms.model.expressions.composedexpression.unaryExpression.Negation;
import worms.model.expressions.composedexpression.unaryExpression.SameTeam;
import worms.model.expressions.composedexpression.unaryExpression.SearchObject;
import worms.model.expressions.composedexpression.unaryExpression.Sine;
import worms.model.expressions.composedexpression.unaryExpression.SquareRoot;
import worms.model.statements.Assignment;
import worms.model.statements.ForEachLoop;
import worms.model.statements.Hold;
import worms.model.statements.IfThenElse;
import worms.model.statements.Jump;
import worms.model.statements.Move;
import worms.model.statements.Print;
import worms.model.statements.Sequence;
import worms.model.statements.Shoot;
import worms.model.statements.Statement;
import worms.model.statements.SwitchWeapon;
import worms.model.statements.Turn;
import worms.model.statements.WhileLoop;
import worms.model.types.BoolType;
import worms.model.types.DoubleType;
import worms.model.types.EntityType;
import worms.model.types.Type;

@SuppressWarnings("unchecked")
public class ProgramFactoryImplementation implements ProgramFactory<Expression<? extends Type>, Statement, Type> {

	@Override
	public Expression<? extends Type> createDoubleLiteral(int line, int column,
			double d) {
		return new DoubleLiteralConstant(new DoubleType(d));
	}

	@Override
	public Expression<? extends Type> createBooleanLiteral(int line,
			int column, boolean b) {
		return new BooleanLiteral(new BoolType(b));
	}

	@Override
	public Expression<? extends Type> createAnd(int line, int column,
			Expression<? extends Type> e1, Expression<? extends Type> e2) {
		return new Conjunction((Expression<BoolType>)e1, (Expression<BoolType>)e2);
	}

	@Override
	public Expression<? extends Type> createOr(int line, int column,
			Expression<? extends Type> e1, Expression<? extends Type> e2) {
		return new Disjunction((Expression<BoolType>)e1, (Expression<BoolType>)e2);
	}

	@Override
	public Expression<? extends Type> createNot(int line, int column,
			Expression<? extends Type> e) {
		return new Negation((Expression<BoolType>)e);
	}

	@Override
	public Expression<? extends Type> createNull(int line, int column) {
		return new ObjectLiteral(null);
	}

	@Override
	public Expression<? extends Type> createSelf(int line, int column) {
		return new Self();
	}

	@Override
	public Expression<? extends Type> createGetX(int line, int column,
			Expression<? extends Type> e) {
		return new GetX((Expression<Type>)e);
	}

	@Override
	public Expression<? extends Type> createGetY(int line, int column,
			Expression<? extends Type> e) {
		return new GetY((Expression<Type>)e);
	}

	@Override
	public Expression<? extends Type> createGetRadius(int line, int column,
			Expression<? extends Type> e) {
		return new GetRadius((Expression<Type>)e);
	}

	@Override
	public Expression<? extends Type> createGetDir(int line, int column,
			Expression<? extends Type> e) {
		return new GetDirection((Expression<Type>)e);
	}

	@Override
	public Expression<? extends Type> createGetAP(int line, int column,
			Expression<? extends Type> e) {
		return new GetActionPoints((Expression<Type>)e);
	}

	@Override
	public Expression<? extends Type> createGetMaxAP(int line, int column,
			Expression<? extends Type> e) {
		return new GetMaxActionPoints((Expression<Type>)e);
	}

	@Override
	public Expression<? extends Type> createGetHP(int line, int column,
			Expression<? extends Type> e) {
		return new GetHitPoints((Expression<Type>)e);
	}

	@Override
	public Expression<? extends Type> createGetMaxHP(int line, int column,
			Expression<? extends Type> e) {
		return new GetMaxHitPoints((Expression<Type>)e);
	}

	@Override
	public Expression<? extends Type> createSameTeam(int line, int column,
			Expression<? extends Type> e) {
		return new SameTeam((Expression<Type>)e);
	}

	@Override
	public Expression<? extends Type> createSearchObj(int line, int column,
			Expression<? extends Type> e) {
		return new SearchObject((Expression<Type>)e);
	}

	@Override
	public Expression<? extends Type> createIsWorm(int line, int column,
			Expression<? extends Type> e) {
		return new IsWorm((Expression<Type>)e);
	}

	@Override
	public Expression<? extends Type> createIsFood(int line, int column,
			Expression<? extends Type> e) {
		return new IsFood((Expression<Type>)e);
	}

	@Override
	public Expression<? extends Type> createVariableAccess(int line,
			int column, String name) {
		return new VariableAcces(name);
	}

	@Override
	public Expression<? extends Type> createVariableAccess(int line,
			int column, String name, Type type) {
		return new VariableAcces(type, name);
	}

	@Override
	public Expression<? extends Type> createLessThan(int line, int column,
			Expression<? extends Type> e1, Expression<? extends Type> e2) {
		return new LessThan((Expression<Type>)e1,(Expression<Type>)e2);
	}

	@Override
	public Expression<? extends Type> createGreaterThan(int line, int column,
			Expression<? extends Type> e1, Expression<? extends Type> e2) {
		return new GreatherThan((Expression<Type>)e1,(Expression<Type>)e2);
	}

	@Override
	public Expression<? extends Type> createLessThanOrEqualTo(int line,
			int column, Expression<? extends Type> e1,
			Expression<? extends Type> e2) {
		return new LessThanOrEqualTo((Expression<Type>)e1,(Expression<Type>)e2);
	}

	@Override
	public Expression<? extends Type> createGreaterThanOrEqualTo(int line,
			int column, Expression<? extends Type> e1,
			Expression<? extends Type> e2) {
		return new GreatherThanOrEqualTo((Expression<Type>)e1,(Expression<Type>)e2);
	}

	@Override
	public Expression<? extends Type> createEquality(int line, int column,
			Expression<? extends Type> e1, Expression<? extends Type> e2) {
		return new Equal((Expression<Type>)e1,(Expression<Type>)e2);
	}

	@Override
	public Expression<? extends Type> createInequality(int line, int column,
			Expression<? extends Type> e1, Expression<? extends Type> e2) {
		return new NotEqual((Expression<Type>)e1,(Expression<Type>)e2);
	}

	@Override
	public Expression<? extends Type> createAdd(int line, int column,
			Expression<? extends Type> e1, Expression<? extends Type> e2) {
		return new Addition((Expression<DoubleType>)e1,(Expression<DoubleType>)e2);
	}

	@Override
	public Expression<? extends Type> createSubtraction(int line, int column,
			Expression<? extends Type> e1, Expression<? extends Type> e2) {
		return new Subtraction((Expression<DoubleType>)e1,(Expression<DoubleType>)e2);
	}

	@Override
	public Expression<? extends Type> createMul(int line, int column,
			Expression<? extends Type> e1, Expression<? extends Type> e2) {
		return new Multiplication((Expression<DoubleType>)e1,(Expression<DoubleType>)e2);
	}

	@Override
	public Expression<? extends Type> createDivision(int line, int column,
			Expression<? extends Type> e1, Expression<? extends Type> e2) {
		return new Division((Expression<DoubleType>)e1,(Expression<DoubleType>)e2);
	}

	@Override
	public Expression<? extends Type> createSqrt(int line, int column,
			Expression<? extends Type> e) {
		return new SquareRoot((Expression<DoubleType>)e);
	}

	@Override
	public Expression<? extends Type> createSin(int line, int column,
			Expression<? extends Type> e) {
		return new Sine((Expression<DoubleType>)e);
	}

	@Override
	public Expression<? extends Type> createCos(int line, int column,
			Expression<? extends Type> e) {
		return new Cosine((Expression<DoubleType>)e);
	}

	@Override
	public Statement createTurn(int line, int column,
			Expression<? extends Type> angle) {
		return new Turn((Expression<DoubleType>)angle);
	}

	@Override
	public Statement createMove(int line, int column) {
		return new Move();
	}

	@Override
	public Statement createJump(int line, int column) {
		return new Jump();
	}

	@Override
	public Statement createToggleWeap(int line, int column) {
		return new SwitchWeapon();
	}

	@Override
	public Statement createFire(int line, int column,
			Expression<? extends Type> yield) {
		return new Shoot((Expression<DoubleType>)yield);
	}

	@Override
	public Statement createSkip(int line, int column) {
		return new Hold();
	}

	@Override
	public Statement createAssignment(int line, int column,
			String variableName, Expression<? extends Type> rhs) {
		return new Assignment(variableName, rhs);
	}

	@Override
	public Statement createIf(int line, int column,
			Expression<? extends Type> condition, Statement then,
			Statement otherwise) {
		return new IfThenElse((Expression<BoolType>)condition, then, otherwise);
	}

	@Override
	public Statement createWhile(int line, int column,
			Expression<? extends Type> condition, Statement body) {
		return new WhileLoop((Expression<BoolType>)condition, body);
	}

	@Override
	public Statement createForeach(int line, int column,
			worms.model.programs.ProgramFactory.ForeachType type,
			String variableName, Statement body) {
		return new ForEachLoop(type, variableName, body);
	}

	@Override
	public Statement createSequence(int line, int column,
			List<Statement> statements) {
		return new Sequence(statements);
	}

	@Override
	public Statement createPrint(int line, int column,
			Expression<? extends Type> e) {
		return new Print((Expression<Type>)e);
	}

	@Override
	public Type createDoubleType() {
		return new DoubleType();
	}

	@Override
	public Type createBooleanType() {
		return new BoolType();
	}

	@Override
	public Type createEntityType() {
		return new EntityType();
	}
	
}