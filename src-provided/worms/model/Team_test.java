package worms.model;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class Team_test {

	private IFacade facade;

	private Random random;

	private World world;

	// X X X X
	// . . . .
	// . . . .
	// X X X X
	private boolean[][] passableMap = new boolean[][] {
			{ false, false, false, false }, { true, true, true, true },
			{ true, true, true, true }, { false, false, false, false } };
	


	@Before
	public void setup() {
		facade = new Facade();
		random = new Random(7357);
		world = facade.createWorld(4.0, 4.0, passableMap, random);
	}

	// NAME
	
	@Test
	public void getName_TestCase() {
 
		Team team = new Team("TheHappyTesters", world);
		assertEquals("TheHappyTesters", team.getName());
	}
	
	@Test
	public void isValidName_LegalCase() {
		assertEquals(true, Team.isValidName("Axe"));
	}
	
	@Test
	public void isValidName_IllegalCase1() {
		assertEquals(false, Team.isValidName("axe"));
	}
	
	@Test
	public void isValidName_IllegalCase2() {
		assertEquals(false, Team.isValidName("Axe5"));
	}
	
	@Test
	public void isValidName_IllegalCase3() {
		assertEquals(false, Team.isValidName("A"));
	}
	
	@Test
	public void isValidName_IllegalCase4() {
		assertEquals(false, Team.isValidName("+++++=====//////"));
	}
	
	@Test
	public void isValidName_IllegalCase5() {
		assertEquals(false, Team.isValidName(""));
	}
	
	@Test
	public void setName_LegalCase() {
		Team team = new Team("TheHappyTesters", world);
		team.setName("Testers");
		assertEquals("Testers", team.getName());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void setName_IllegalCase() {
 
		Team team = new Team("TheHappyTesters", world);
		team.setName("happytesters");
	}
	
	// WORM
	
	
	
	@Test
	public void canHaveAsWorm_testCase() {
		if (world.findRandomAdjacentLocation(0.50)) {
		}
		
		Worm worm = new Worm(world.getAdjacentLocationX(), world.getAdjacentLocationY(), world, 0.50, 0.50, "Kiki", null);
		Team happyTesters = new Team("TheHappyTesters", world);
		
		assertFalse(happyTesters.canHaveAsWorm(worm));
	}
	
	
	
	// WORLD
	
	@Test
	public void getWorld_testCase() {
 
		Team team = new Team("TheHappyTesters", world);		
		assertEquals(true, world == team.getWorld());
	}
	
	@Test
	public void canHaveAsWorld_legalcase() {
 
		Team team = new Team("TheHappyTesters", world);
		assertEquals(true, team.canHaveAsWorld(world));
	}
	
	@Test
	public void hasProperWorld_legalcase() {
 
		Team team = new Team("TheHappyTesters", world);
		assertEquals(true, team.hasProperWorld());
	}
	
	
	
}
