package worms.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class of projectiles, this is the super class of RifleBullet and BazookaBullet.
 * 
 * @invar	Each projectile must have a valid x-coordinate.
 * 			| isValidCoordinate(getCoordinateX())
 * @invar 	Each projectile must have a valid y-coordinate.
 * 			| isValidCoordinate(getCoordinateY())
 * @invar	Each projectile must have a valid direction.
 * 			| isValidDirection(getDirection())
 * @invar	Each projectile must have a valid radius.
 * 			| isValidRadius(getRadius())
 * @invar  	Each projectile must have a proper weapon.
 *       	| hasProperWeapon()
 * @invar	Each projectile must have a proper world.
 * 			| hasProperWorld()
 * 
 * @version  2.0
 * @author   Axel Lemmens & Maarten Rimaux
 */
public abstract class Projectile extends GameObject {
	
	//----------------------- CONSTRUCTOR --------------------------//
	
	/**
	 *  Initialise this new projectile with the given parameters.
	 * 
	 * @param	xCoordiante
	 * 			The provided value for the x-coordinate in meters.
	 * @param	yCoordinate
	 * 			The provided value for the y-coordinate in meters.
	 * @param	direction
	 * 			The provided value for the direction in radians.
	 * @param	weapon
	 * 			The provided value for the weapon of this new projectile.
	 * @param	world
	 * 			The provided value for the world of this new projectile.
	 * @pre		The provided direction must be a valid value.
	 * 			| isValidDirection(direction)
	 * @effect	The new value of the x-coordinate is set to the provided value "xCoordinate"
	 * 			| setCoordinateX(xCoordinate)
	 * @effect	The new value of the y-coordinate is set to the provided value "yCoordinate"
	 * 			| setCoordinateY(yCoordinate)
	 * @effect	The new value of the direction is set to the provided value "direction"
	 * 			| setDirection(direction)
	 * @effect	The new world of this new projectile is set to the provided "world"
	 * 			| setWorld(world)
	 * @effect	The new weapon of this new projectile is equal to the provided "weapon"
	 * 			| new.getWeapon() == weapon
	 * @post   	The number of projectiles for the given world is
	 *         	incremented by 1.
	 *       	| (new world).getProjectiles().size() == world.getProjectiles().size() + 1
	 * @post   	The given world has this new projectile as its very last projectile.
	 *      	| (new world).getProjectiles().get(world.getProjectiles().size()+1) == this
	 * @post   	The number of projectiles for the given weapon is
	 *         	incremented by 1.
	 *       	| (new weapon).getProjectiles().size() == weapon.getProjectiles().size() + 1
	 * @post   	The given weapon has this new projectile as its very last projectile.
	 *      	| (new weapon).getProjectiles().get(weapon.getProjectiles().size()+1) == this
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided value is not a valid coordinate.
	 * 			| ! isValidCoordinate(xCoordinate)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided value is not a valid coordinate.
	 * 			| ! isValidCoordinate(yCoordinate)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when this new projectile cannot have the given weapon as
	 *         	its weapon.
	 * 			| ! canHaveAsWeapon(weapon)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when this new projectile cannot have the given world as
	 *         	its world.
	 * 			| ! canHaveAsWorld(world)
	 */
	public Projectile(double xCoordinate, double yCoordinate, World world, double direction, Weapon weapon)
			throws IllegalArgumentException {
		super(xCoordinate, yCoordinate, world);
		this.setDirection(direction);
		this.setWeapon(weapon);
		weapon.addProjectile(this);
	}
	
	//-------------------- ORIENTATION ----------------------------//
	
	/**
	 * A method that returns the direction of this projectile.
	 */
	@Basic @Raw
	public double getDirection() {
		return direction;
	}
	
	/**
	 * A method to check whether a direction is valid.
	 * 
	 * @param 		direction 
	 * 				The direction to check.
	 * @return 		The result should be a valid number.
	 * 				| result == !double.isNaN(direction) 
	 * 				| && (direction < Double.POSITIVE_INFINITY) 
	 * 				| && (direction > Double.NEGATIVE_INFINITY)
	 */
	public static boolean isValidDirection(double direction){
		return (!Double.isNaN(direction) 
				&& (direction < Double.POSITIVE_INFINITY) 
				&& (direction > Double.NEGATIVE_INFINITY));	
	}
	
	/**
	 * A method to set the direction of this projectile to the provided value.
	 * 
	 * @param 		direction
	 * 				The new direction for the orientation of this projectile.
	 * @pre			The provided value must be a valid value.
	 * 				| isValidDirection(direction)
	 * @post		The direction of this projectile is set to the provided value.
	 * 				| new.getDirection() == direction
	 */
	@Raw
	public void setDirection(double direction) { 
		assert(isValidDirection(direction));
		this.direction = direction;
	}
	
	/**
	 * Variable registering the direction of this projectile.
	 */
	private double direction;
	
	//---------------------- RADIUS ------------------------------//
	
	/**
	 * A method that returns the radius of this projectile expressed in meters.
	 */
	@Basic @Raw
	public abstract double getRadius();
	
	/**
	 * A method to check whether a given value for the radius is valid.
	 * 
	 * @param		radius
	 * 				The radius to check.
	 * @return 		The result must be a valid number.
	 * 				| result == double.isNaN(radius) 
	 * 				| && (radius < Double.POSITIVE_INFINITY)
	 * 				| && (radius > Double.NEGATIVE_INFINITY))
	 */
	public Boolean isValidRadius(double radius) {
		return (!Double.isNaN(radius) 
				&& (radius < Double.POSITIVE_INFINITY)
				&& (radius > Double.NEGATIVE_INFINITY));
	}
	
	//---------------------- MASS --------------------------------//
	
	/**
	 * A method to return the mass of this projectile expressed in kilogram.
	 */
	@Basic @Raw
	public abstract double getMass();
	
	/**
	 * A method to return the density of this projectile.
	 */
	@Basic @Immutable
	public double getDensity(){
		return 7800.0;
	}
	
	//--------------------- SHOOT ----------------------------------//
	
	/**
	 * A method to return the shooting cost of this projectile. 
	 */
	@Basic @Raw
	public abstract int getShootingCost();
	
	/**
	 * A method to return the hit cost of this projectile. 
	 */
	@Basic @Raw
	public abstract int getHitCost();
	
	/**
	 * A method to return the force invoked on this projectile.
	 */
	@Basic @Raw
	public abstract double getForce();	
	
	/**
	 * A method to return the acceleration of the earth.
	 */
	@Basic @Immutable
	public double getAccelerationOfEarth(){
		return 9.80665;
	}
	
	/**
	 * A method to return the maximum shooting cost of this projectile. 
	 */
	@Basic @Immutable
	public int getMaximumShootingCost(){
		return 80;
	}
	
	/**
	 * A method to returns the velocity of this projectile.
	 * 
	 * @return		The velocity of this worm.
	 * 				| result == (getForce()/getMass())*0.5
	 */
	public double getVelocity(){
		return (((getForce()/getMass()))*0.5);
	}
	
	/**
	 * A method to check whether this projectile can be shot.
	 * 
	 * @return 		The method returns true if the current action points,
	 * 				of the worm of the weapon of this projectile, are bigger
	 * 				than maximum shooting cost and if the worm of the weapon
	 * 				of this projectile is located on passable terrain.
	 * 				| result == (this.getWeapon().getWorm().getCurrentActionPoints() >= getMaximumShootingCost()) 
	 * 				| 			&& ( this.getWeapon().getWorm().getWorld().isPassable(
	 * 				|				 this.getWeapon().getWorm().getCoordinateX(), 
	 * 				|				 this.getWeapon().getWorm().getCoordinateY(), 
	 * 				|				 this.getWeapon().getWorm().getRadius()))
	 */
	public boolean canShoot(){
		return (this.getWeapon().getWorm().getCurrentActionPoints() >= getMaximumShootingCost()) 
				 && ( this.getWeapon().getWorm().getWorld().isPassable(
						 this.getWeapon().getWorm().getCoordinateX(), 
						 this.getWeapon().getWorm().getCoordinateY(),
						 this.getWeapon().getWorm().getRadius()));
	}

	/**
	 *  A method to shoot this projectile.
	 *  
	 * @param	timeStep
	 * 			The timeStep An elementary time interval during which you may
	 * 			assume that the projectile will not completely move through a 
	 * 			piece of impassable terrain.
	 * @effect	The x-coordinate is set to the founded location.
	 * 			| setCoordinateX(this.shootStep(getShootTime(timeStep))[0])
	 * @effect	The y-coordinate is set to the founded location.
	 * 			| setCoordinateY(this.shootStep(getShootTime(timeStep))[1])
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when this projectile cannot be shot.
	 * 			| ! canShoot()
	 */
	public void shoot(double timeStep) throws IllegalArgumentException{
		if(!this.canShoot())
			throw new IllegalArgumentException();
		double[] inFlightCoordinates = this.shootStep(getShootTime(timeStep));
		setXCoordinate(inFlightCoordinates[0]);
		setYCoordinate(inFlightCoordinates[1]);
	}
	
	/**
	 * A method to determine the time that this projectile needs until it hits terrain, 
	 * hits a worm or leaves the world.
	 * 
	 * @param	timeStep
	 * 			timeStep An elementary time interval during which you may assume
	 *          that the worm will not completely move through a piece of impassable terrain.
	 * @return	the time needed for this projectile to hit terrain, a worm or leaves the world.
	 * 			| double time = 0
	 * 			| double[] inFlightCoordinates = this.shootStep(time)
	 * 			| while( (this.getWorld().isPassable(inFlightCoordinates[0], inFlightCoordinates[1], getRadius())) 
	 *			| 		&& !(this.wormCollisionDetection(inFlightCoordinates[0], inFlightCoordinates[1]))
	 *			|		&& validWorldCoordinates(inFlightCoordinates[0], inFlightCoordinates[1])
	 *			|		time = time + timeStep
	 *			|		inFlightCoordinates = this.shootStep(time)
	 *			|	}
	 *			|	return time				
	 */
	public double getShootTime(double timeStep) {
		double time = 0;
		double[] inFlightCoordinates = this.shootStep(time);
		while( (this.getWorld().isPassable(inFlightCoordinates[0], inFlightCoordinates[1], getRadius())) 
				&& !(wormCollisionDetection(inFlightCoordinates[0], inFlightCoordinates[1],true))
				&& 	validWorldCoordinates(inFlightCoordinates[0], inFlightCoordinates[1])){
			time = time + timeStep;
			inFlightCoordinates = this.shootStep(time);
		}
		return time;
	}
	
	/**
	 *  A method to check whether the x-coordinate and y-coordinate 
	 *  are valid in the world of this projectile
	 *  
	 * @param	xCoordinate
	 * 			The x-coordinate to check.
	 * @param	yCoordinate
	 * 			The y-coordinate to check.
	 * @return	result is true when the coordinates are in the boundaries
	 * 			 of the world of this projectile.
	 * 			| result == (xCoordinate >= 0) 
	 *			|      		&& (xCoordinate <= this.getWorld().getWidth()) 
	 *			|			&& (yCoordinate >= 0) 
	 *			|			&& (yCoordinate <= this.getWorld().getHeight())
	 */
	public boolean validWorldCoordinates(double xCoordinate, double yCoordinate){
		return (xCoordinate >= 0) 
				&& (xCoordinate <= this.getWorld().getWidth()) 
				&& (yCoordinate >= 0) 
				&& (yCoordinate <= this.getWorld().getHeight());
	}
	
	/**
	 * A method to compute the in-flight positions x-coordinate and 
	 * y-coordinate at any time after the projectile is shot.
	 * 
	 * @param 	time
	 * 			The time after the projectile is shot.
	 * @return	An array containing the new values for the X and Y coordinate at time.
	 *			|result == 
	 *			| new.getCoordinateX = this.getCoordinateX() + (((this.getVelocity()*Math.cos(this.getDirection()))*time))
	 *			| new.getCoordinateY = this.getCoordinateY() + ((this.getVelocity()*Math.sin(this.getDirection())*time) - ((this.getAccelerationOfEarth()*Math.pow(time, 2))/2))
	 *			| double[] result = {new.getCoordinateX, new.getCoordinateY}
	 * @throws 	IllegalArgumentException
	 * 			The method throws the exception when the projectile cannot be shot.
	 * 			| (!this.canShoot())
	 */
	public double[] shootStep(double time) throws IllegalArgumentException{
		if(! this.canShoot())
			throw new IllegalArgumentException("The projectile cannot be fired");
		double x = this.getCoordinateX() + (((this.getVelocity()*Math.cos(this.getDirection()))*time));
		double y = this.getCoordinateY() + ((this.getVelocity()*Math.sin(this.getDirection())*time) - ((this.getAccelerationOfEarth()*Math.pow(time, 2))/2));
		double[] result = {x, y};
		return result;
	}
	
	/**
	 * A method to check whether this projectile is active or not.
	 * 
	 * @post	If the result is false and only if it is false, this
	 * 			projectile is terminated.
	 * 			| new.isTerminated() == true
	 * @return	The result is true if this projectile has not hit a terrain,
	 * 			a worm or has not left the world.
	 * 			| if( ! this.getWorld().isPassable(this.getCoordinateX(), this.getCoordinateY(), this.getRadius()) 
	 * 			|     | | ! this.validWorldCoordinates(this.getCoordinateX(), this.getCoordinateY()  )
	 * 			| 		then this.terminate() && result == false
	 * 			| else 
	 * 			|		if( wormCollisionDetection(this.getCoordinateX(), this.getCoordinateY(), false) )
	 *			|		 	then this.terminate() && result == false
	 *			| else 
	 *			|		result == true
	 */
	public boolean isActive(){
		if( ! this.getWorld().isPassable(this.getCoordinateX(), this.getCoordinateY(), this.getRadius())
			|| ! this.validWorldCoordinates(this.getCoordinateX(), this.getCoordinateY()) ){
				this.terminate();
				return false;
		} else if(wormCollisionDetection(this.getCoordinateX(), this.getCoordinateY(), false)){
				this.terminate();
				return false;
		} else {
				return true;
		}
	}
	
	/**
	 * A method to check whether this projectile has hit a worm.
	 * 
	 * @param	xCoordinate
	 * 			The x-coordinate to check.
	 * @param 	yCoordinate
	 * 			The y-coordinate to check.
	 * @param 	decreaseHitPoints	
	 * 			The boolean to determine if the worm that has been hit by
	 * 			this projectile must loose hit points.	
	 * @post	If the result is true and the given boolean decreaseHitPoints
	 * 			is true then the worm that has been hit by this projectile,
	 * 			his hit points are decreased with the hit cost of this projectile.
	 * 			|(I is the index of the worm that has been hit)
	 * 			| new.getWorld().getWorms().get(I).getCurrentHitPoints() ==
	 * 			| this.getWorld().getWorms().get(I).getCurrentHitPoints() - this.getHitCost()
	 * @return	The result is true when a worm of the world of this projectile has been hit.
	 * 			| for each I in 0..this.getWorld().getWorms().size()-1
	 * 			|	if( (Math.sqrt( Math.pow(xCoordinate - this.getWorld().getWorms().get(i).getCoordinateX(), 2) +
	 *			|			    	Math.pow(yCoordinate - this.getWorld().getWorms().get(i).getCoordinateY(), 2) )) 
	 *			|		<= this.getWorld().getWorms().get(i).getRadius() )
	 *			|			then result == true
	 */
	public boolean wormCollisionDetection(double xCoordinate, double yCoordinate, boolean decreaseHitPoints) {
		for(int i = 0; i < this.getWorld().getWorms().size(); i++)
			if(this.getWorld().getWorms().get(i) != this.getWeapon().getWorm()){
				double distanceBetweenBulletAndOtherWorms = Math.sqrt( Math.pow(xCoordinate - this.getWorld().getWorms().get(i).getCoordinateX(), 2) + 
																	   Math.pow(yCoordinate - this.getWorld().getWorms().get(i).getCoordinateY(), 2));
				if(distanceBetweenBulletAndOtherWorms < this.getWorld().getWorms().get(i).getRadius()){
					if(decreaseHitPoints)
						this.getWorld().getWorms().get(i).setCurrentHitPoints(this.getWorld().getWorms().get(i).getCurrentHitPoints()-this.getHitCost());
					return true;
				}
			}
		return false;
	}
	
	//--------------------- WEAPON ----------------------------------//

	/** 
	 * Return the current weapon of this projectile.
	 */
	@Basic @Raw 
	public Weapon getWeapon() {
		return this.weapon;
	}
	
	/**
	 * Check whether this projectile can have the given weapon as its current weapon.
	 * 
	 * @param	weapon
	 *          The weapon to check.
	 * @return	If this projectile is not yet terminated, true if and
	 *          only if the given weapon is effective.
	 *        	| if (! isTerminated())
	 *       	|   then result == (weapon != null) 
	 * @return	If this projectile is terminated, true if and only if
	 *          the given weapon is not effective.
	 *        	| if (this.isTerminated())
	 *       	|   then result == (weapon == null)
	 */
	@Raw
	public boolean canHaveAsWeapon(Weapon weapon) {
		if (isTerminated())
			return (weapon == null);
		return (weapon != null);
	}
	
	/**
	 * Check whether this projectile has a proper weapon.
	 * 
	 * @return	True if and only if this projectile can have its weapon as its
	 * 			weapon, and if this projectile is terminated or the weapon of
	 *          this projectile has this projectile as one of its projectiles.
	 *       	| result ==
	 *        	|   canHaveAsWeapon(getWeapon()) &&
	 *       	|   ( isTerminated() || getWeapon().hasAsProjectile(this))
	 */
	public boolean hasProperWeapon() {
		return canHaveAsWeapon(getWeapon()) &&
				 ( isTerminated() || getWeapon().hasAsProjectile(this));
	}
	
	/** 
	 * Register the given weapon as the weapon of this projectile.
	 * 
	 * @param	weapon
	 *          The weapon to be registered as the weapon of this projectile.
	 * @post   	The weapon of this projectile is the same as the given weapon.
	 *       	| new.getWeapon() == weapon
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided weapon is not valid.
	 * 			| ! canHaveAsWeapon(weapon)
	 */
	@Raw
	private void setWeapon(Weapon weapon) throws IllegalArgumentException {
		if( !canHaveAsWeapon(weapon) )
			throw new IllegalArgumentException("The provided weapon is not a valid weapon.");
		this.weapon = weapon;
	}
	
	/**
	 * Variable referencing the weapon of this projectile.
	 */
	private Weapon weapon;
	
	//--------------------- TERMINATE ----------------------------------//
	
		
	/**
	 * Terminate this projectile.
	 * 
	 * @post	This projectile is terminated.
	 *       	| new.isTerminated()
	 * @post   	If this projectile was not yet terminated, this projectile
	 *        	is no longer one of the projectiles of the weapon to which
	 *        	this projectile belonged.
	 *     		| if (! isTerminated())
	 *    	    |   then ! (new getWeapon()).hasAsProjectile(this))
	 * @post  	If this projectile was not yet terminated, the number of
	 *      	projectiles of the weapon to which this projectile belonged is
	 *        	decremented by 1.
	 *     		| if (! isTerminated())
	 *      	|   then (new getWeapon()).getProjectiles().size() ==
	 *      	|            this.getWeapon().getProjectiles().size() - 1
	 * @post  	If this projectile was not yet terminated, all projectiles
	 *        	of the weapon to which this projectile belonged registered at an
	 *        	index beyond the index at which this projectile was registered,
	 *        	are shifted one position to the left.
	 *       	| for each I,J in ...getWeapon().getProjectiles().size()-1:
	 *       	|   if ( (getWeapon().getProjectiles().get(I) == weapon) and (I < J) )
	 *       	|     then (new getWeapon()).getProjectiles().get(J-1) == getWeapon().getProjectiles().get(J)
	 * @post   	If this projectile was not yet terminated, this projectile
	 *        	is no longer one of the projectiles of the world to which
	 *        	this projectile belonged.
	 *     		| if (! isTerminated())
	 *    	    |   then ! (new getWorld()).hasAsProjectile(this))
	 * @post  	If this projectile was not yet terminated, the number of
	 *      	projectiles of the world to which this projectile belonged is
	 *        	decremented by 1.
	 *     		| if (! isTerminated())
	 *      	|   then (new getWorld()).getProjectiles().size() ==
	 *      	|            this.getWorld().getProjectiles().size() - 1
	 * @post  	If this projectile was not yet terminated, all projectiles
	 *        	of the world to which this projectile belonged registered at an
	 *        	index beyond the index at which this projectile was registered,
	 *        	are shifted one position to the left.
	 *       	| for each I,J in ...getWorld().getProjectiles().size()-1:
	 *       	|   if ( (getWorld().getProjectiles().get(I) == projectile) and (I < J) )
	 *       	|     then (new getWeapon()).getProjectiles().get(J-1) == getWeapon().getProjectiles().get(J)
	 * 
	 */
	@Override
	public void terminate() {
		if (!isTerminated()) {
			setState(State.TERMINATED);
			Weapon oldWeapon = getWeapon();
			this.setWeapon(null);
			oldWeapon.removeProjectile(this);
			World oldWorld = getWorld();
			this.setWorld(null);
			oldWorld.removeGameObject(this);
		}
	}
		
}