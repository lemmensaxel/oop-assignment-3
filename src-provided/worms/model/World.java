package worms.model;

import java.util.ArrayList;
import java.util.Random;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class of worlds.
 * 
 * @invar	Each world must have a valid width and height.
 * 			| isValidSizeOfWorld(width, height)
 *
 * @version	2.0
 * @author Axel Lemmens & Maarten Rimaux 
 *
 */
public class World {
	
	//<------------------------------------------------------------------CONSTRUCTOR------------------------------------------------------------>
	
	/**
	 * Initialise this new world with the given parameters. 
	 *  
	 * @param 	width
	 * 			The width of this new world in meters.
	 * @param 	height
	 * 			The height of this new world in meters.
	 * @param	passableMap 
	 * 			A rectangular matrix indicating which parts of the terrain are passable and impassable. 
	 * @param	random 
	 * 			A random number generator, seeded with the value obtained from the command line or from GUIOptions,
	 *			that can be used to randomise aspects of this world in a repeatable way.
	 * @effect	The new value of the passableMap is set to the provided value "passableMap"
	 * 			| setPassableMap(passableMap)
	 * @effect	The new value of the random is set to the provided value "random"
	 * 			| setRandom(random)
	 * @post	The width is set to the provided width.
	 * 			| new.getWidth() == width
	 * @post	The height is set to the provided height.
	 * 			| new.getHeight() == height
	 * @throws 	IllegalArgumentException
	 * 			An exception is thrown when the provided width and height are not valid.
	 * 			| ! isValidSizeOfWorld(width,height)
	 */
	public World(double width, double height, boolean[][] passableMap, Random random) throws IllegalArgumentException {
		if (!isValidSizeOfWorld(width, height)) 
			throw new IllegalArgumentException("Invalid width or height.");
		this.width = width;
		this.height = height;
		this.setPassableMap(passableMap);
		this.setRandom(random);
	}
	
	//<---------------------------------------------------------------WIDTH & HEIGHT--------------------------------------------------------->
	
	/**
	 * A method to return the width of the world in meters.
	 */
	@Basic @Raw
	public double getWidth() {
		return width;
	}
	
	/**
	 * A method to return the height of the world in meters.
	 */
	@Basic @Raw
	public double getHeight() {
		return height;
	}
	
	/**
	 * A method to check whether the size of this world is valid.
	 * 
	 * @param 	width
	 * 			The width to check.
	 * @param 	height
	 * 			The height to check.
	 * @return 	The result should be a width and a height between 0 and Double.MAX_VALUE (both inclusive).
	 * 		  	| result == (width >= 0) 
	 * 		  	| && (width <= Double.MAX_VALUE) 
	 * 		  	| && (height >= 0) 
	 * 		  	| && (height <= Double.MAX_VALUE)
	 */
	public static boolean isValidSizeOfWorld(double width, double height) { 
		return ((width >= 0) && (width <= Double.MAX_VALUE) 
				&& (height >= 0) && (height <= Double.MAX_VALUE));									 
	}
	
	/**
	 * Variable registering the width of this world in meters.
	 */
	private double width;
	
	/**
	 * Variable registering the height of this world in meters.
	 */
	private double height;
	
	//<----------------------------------------------TEAMS---------------------------------------------------->
	
	/**
	 * A method to return a list of all the teams in this world.
	 */
	@Basic @Raw
	public ArrayList<Team> getTeams(){
		return teamObjects;
	}
	
	/**
	 * A method to return the max number of teams this world can have.
	 */
	@Basic @Immutable
	public int getMaxNumberOfTeams(){
		return 10;
	}
	
	/**
	 * A method to add a team to this world.
	 * 
	 * @param	team
	 * 			The team to be added.
	 * @pre		The given team is effective and already references
	 *     	    this world, and this world does not yet have the given
	 *        	team as one of its teams.
	 *      	| (team != null) && (team.getWorld() == this) &&
	 *      	| (! this.hasAsTeam(team))
	 * @post    The number of teams for this world is
	 *          incremented by 1.
	 *       	| new.getTeams().size() == this.getTeams().size() + 1
	 * @post    A new team is added to this world.
	 *       	| new.getTeams().get(getTeams().size()+1) == team
	 */
	@Basic @Raw
	public void addTeam(Team team){
		assert (team != null) && (team.getWorld() == this)
				&& (!this.hasAsTeam(team));
		teamObjects.add(team);
	}
	
	/**
	 * Check whether this world has the given team as one of its
	 * teams.
	 * 
	 * @param  	team
	 * 		   	The team to check.
	 * @return	The result is true when the provided team is a team of this world.
	 *       	| result ==  getTeams().contains(team)
	 */
	@Raw
	public boolean hasAsTeam(Team team) {
		return getTeams().contains(team);
	}
	
	/**
	 * Variable referencing a list collecting all the teams in this world.
	 */
	private ArrayList<Team> teamObjects = new ArrayList<Team>();
	
	//<-------------------------------------------------------GAMEOBJECTS------------------------------------------------------->
	
	/**
	 * A method to return a list of all the gameObjects in this world.
	 */
	@Basic @Raw
	public ArrayList<GameObject> getGameObjects() {
		return gameObjects;
	}
	
	/**
	 * Check whether this world can have the given gameObject
	 * as one of its gameObjects.
	 * 
	 * @param	gameObject
	 *        	The gameObject to check.
	 * @return	True if and only if the given gameObject is effective
	 *        	and already references this world, and this world
	 *        	does not yet have the given gameObject as one of its gameObjects.
	 *       	| result ==
	 *       	|   (gameObject != null) && (gameObject.getWorld() == this)
				|		&& (!this.hasAsGameObject(gameObject))
	 */
	@Raw
	public boolean canHaveAsGameObject(GameObject gameObject) {
		return (gameObject != null) && (gameObject.getWorld() == this)
				&& (!this.hasAsGameObject(gameObject));
	}
	
	/**
	 * Check whether this world has the given gameObject as one of its
	 * gameObjects.
	 * 
	 * @param  	gameObject
	 * 		   	The gameObject to check.
	 * @return	The result is true when the provided gameObject is a gameObject of this world.
	 *       	| result == getGameObjects().contains(gameObject)
	 */
	@Raw
	public boolean hasAsGameObject(@Raw GameObject gameObject) {
		return getGameObjects().contains(gameObject);
	}
	
	/**
	 * Add the given gameObject to the list of gameObjects of this world.
	 * 
	 * @param	gameObject
	 *         	The gameObject to be added.
	 * @post	The number of gameObjects of this world is
	 *        	incremented by 1.
	 *      	| new.getGameObjects().size() == this.getGameObjects().size() + 1
	 * @post   	This world has the given gameObject as its very last gameObject.
	 *       	| new.getGameObjects().get(getGameObjects().size()+1) == gameObject
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided gameObject is not a valid gameObject.
	 * 			| ! canHaveAsGameObject(gameObject)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when u want to add a gameObject( and the gameObject is not
	 * 			a projectile) if the game is started.
	 * 			| isGameStarted()
	 */
	@Basic @Raw
	public void addGameObject(GameObject gameObject) throws IllegalArgumentException{
		if(!canHaveAsGameObject(gameObject))
			throw new IllegalArgumentException("The provided gameObject is not a valid gameObject.");
		if( !(gameObject instanceof Projectile) && isGameStarted())
			throw new IllegalArgumentException("The game is started u cannot add any gameObjects.");
		gameObjects.add(gameObject);
	}
	
	/**
	 * 	A method to remove the given gameObject from the list of gameObjects of this world.
	 * 
	 * @param	gameObject
	 *         	The gameObject to be removed.
	 * @post   	The number of gameObjects of this world is
	 *         	decremented by 1.
	 *       	| new.getGameObjects().size() == getGameObjects().size() - 1
	 * @post    This world has no longer the given gameObject as
	 *      	one of its gameObject.
	 *       	| ! new.hasAsGameObject(gameObject)
	 * @post	All gameObjects registered at an index beyond the index at
	 *        	which the given gameObject was registered, are shifted
	 *       	one position to the left.
	 *       	| for each I,J in 0..getGameObjects().size()-1:
	 *      	|   if ( (getGameObjects().get(I) == gameObject) and (I < J) )
	 *      	|     then new.getGameObjects().get(J-1) == getGameObjects().get(J)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the gameObject doesn't exists or
	 * 			if this world has not the given gameObject as one of its gameObjects or
	 * 			if the given gameObject his world still exists.
	 * 			| (gameObject == null) || !this.hasAsGameObject(gameObject) || !(gameObject.getWorld() == null)
	 */
	@Raw
	public void removeGameObject(GameObject gameObject) throws IllegalArgumentException {
		if( (gameObject == null) || !this.hasAsGameObject(gameObject) || !(gameObject.getWorld() == null) )
			throw new IllegalArgumentException("The provided gameObject is not a valid gameObject.");
		getGameObjects().remove(gameObject);
	}
	
	/**
	 * 	A method to return a copy of all the gameObjects.
	 */
	public ArrayList<GameObject> getCopyOfGameObjects() {
		ArrayList<GameObject> gameObjects = new ArrayList<GameObject>();
		for(int i = 0; i < getGameObjects().size(); i++){
			gameObjects.add(getGameObjects().get(i));
		}
		return gameObjects;
	}
	
	/**
	 * Variable referencing a list collecting all the food in this world.
	 */
	private ArrayList<GameObject> gameObjects = new ArrayList<GameObject>();
	
	//<----------------------------------------------------------FOOD---------------------------------------------------------->
	
	/**
	 * A method to return a list of all the food in this world.
	 */
	@Basic @Raw
	public ArrayList<Food> getFood() {
		ArrayList<Food> foodObjects = new ArrayList<Food>();
		for(GameObject o : getCopyOfGameObjects())
			if(o instanceof Food)
				foodObjects.add((Food)o);
		return foodObjects;
	}

	//<----------------------------------------------------- ----WORMS--------------------------------------------------------->
		
	/**
	 * A method to return a list of all the worms in this world.
	 */
	@Basic @Raw
	public ArrayList<Worm> getWorms() {
		ArrayList<Worm> wormObjects = new ArrayList<Worm>();
		for(GameObject o : getCopyOfGameObjects())
			if(o instanceof Worm)
				wormObjects.add((Worm)o);
		return wormObjects;
	}
	
	/**
	 * A method to return the active worm in this world.
	 */
	@Basic @Raw
	public Worm getCurrentWorm(){
		return currentWorm;
	}
	
	/**
	 * A method to set the current worm.
	 * 
	 * @param 	newCurrentWorm
	 * 			The new current worm.
	 * @post	The current worm is set to the provided worm.
	 * 			| new.getCurrentWorm() == newCurrentWorm
	 */
	@Raw
	public void setCurrentWorm(Worm newCurrentWorm) {
		this.currentWorm = newCurrentWorm;
	}
	
	/**
	 * Variable registering the currentWorm of this world.
	 */
	private Worm currentWorm;
	
	//<----------------------------------------------------------PROJECTILES---------------------------------------------------------->
	
	/**
	 * A method to return a list of all the projectiles in this world.
	 */
	@Basic @Raw
	public ArrayList<Projectile> getProjectiles() {
		ArrayList<Projectile> projectileObjects = new ArrayList<Projectile>();
		for(GameObject o : getCopyOfGameObjects())
			if(o instanceof Projectile)
				projectileObjects.add((Projectile) o);
		return projectileObjects;
	}
	
	/**
	 * A method to return the active projectile in this world, or null if no active projectile exists.
	 */
	public Projectile getActiveProjectile(){
		return getProjectiles().get(0);
	}
	
	//<----------------------------------------------------------RANDOM--------------------------------------------------------------->
 
	/**
	 * A method to return a random.
	 */
	@Basic @Raw
	public Random getRandom(){
		return random;
	}
	
	/**
	 * A method to set the random of this world.
	 * 
	 * @param	random
	 * 			The new random of this world.
	 * @post	The new value of the random is set to the provided value.
	 * 			| new.getRandom() == random
	 */
	@Raw
	public void setRandom(Random random){
		this.random = random;
	}
	
	/**
	 * Variable registering the random of this world.
	 */
	private Random random;
	
	//<--------------------------------------------------------LOCATION--------------------------------------------------------------->

	/**
	 * A method that returns a rectangular matrix indicating which parts of the terrain are passable and impassable.
	 */
	@Basic @Raw
	public boolean[][] getPassable() {
		return passableMap ;
	}
	
	/**
	 * A method to set the passableMap of this world.
	 * 
	 * @param	passableMap
	 * 			The new passableMap of this world.
	 * @post	The new value of the passableMap is set to the provided value.
	 * 			| new.getPassable() == passableMap
	 */
	@Raw
	public void setPassableMap(boolean[][] passableMap) {
		this.passableMap = passableMap;
	}
	
	/**
	 * A method to check whether the given circular region of the given world,
	 * defined by the given center coordinates and radius,
	 * is passable and adjacent to impassable terrain. 
	 * 
	 * @param	xCoordinate
	 * 			The x-coordinate of the center of the circle to check.
	 * @param	yCoordinate
	 * 			The y-coordinate of the center of the circle to check.
	 * @param	radius
	 * 			The radius of the circle to check.
	 * @post	if the parameter xCoordinate is smaller than zero, the xCoordinate equals to
	 * 			the absolute value of xCoordinate.
	 * 			| if(xCoordinate < 0)
	 * 			| 	then xCoordinate = Math.abs(xCoordinate)
	 * @post	if the parameter yCoordinate is smaller than zero, the yCoordinate equals to
	 * 			the absolute value of yCoordinate.
	 * 			| if(yCoordinate < 0)
	 * 			| 	then yCoordinate = Math.abs(yCoordinate)
	 * @post	if the parameter xCoordinate is bigger than the width of the world, the xCoordinate equals to
	 * 			the width of the world.
	 * 			| if(xCoordinate > getWidth())
	 * 			|	then xCoordinate = getWidth()
	 * @post	if the parameter yCoordinate is bigger than the height of the world, the yCoordinate equals to
	 * 			the height of the world.
	 * 			| if(yCoordinate > getHeight())
	 * 			|	then yCoordinate = getHeight()
	 * @return	The result is true if the given region is passable and
	 * 			adjacent to impassable terrain, false otherwise.
	 * 			| result == isPassable(xCoordinate, yCoordinate, radius) && !isPassable(xCoordinate, yCoordinate, radius+radius*0.1)
	 */
	public boolean isAdjacentToImpassableTerrain(double xCoordinate, double yCoordinate, double radius){
		if(xCoordinate < 0)
			xCoordinate = Math.abs(xCoordinate);
		if(yCoordinate < 0)
			yCoordinate = Math.abs(yCoordinate);
		if(xCoordinate > getWidth())
			xCoordinate = getWidth();
		if(yCoordinate >getHeight())
			yCoordinate = getHeight();
		if( radius < 0);
			radius = Math.abs(radius);
		return isPassable(xCoordinate, yCoordinate, radius) && !isPassable(xCoordinate, yCoordinate, radius+(radius*0.1));
	}
	
	/**
	 * A method to check whether the given circular region of the given world,
	 * defined by the given center coordinates and radius,
	 * is impassable.
	 * 
	 * @param	xCoordinate
	 * 			The x-coordinate of the center of the circle to check.
	 * @param	yCoordinate
	 * 			The y-coordinate of the center of the circle to check.
	 * @param	radius
	 * 			The radius of the circle to check
	 * @post	if the parameter xCoordinate is smaller than zero, the xCoordinate equals to
	 * 			the absolute value of xCoordinate.
	 * 			| if(xCoordinate < 0)
	 * 			| 	then xCoordinate = Math.abs(xCoordinate)
	 * @post	if the parameter yCoordinate is smaller than zero, the yCoordinate equals to
	 * 			the absolute value of yCoordinate.
	 * 			| if(yCoordinate < 0)
	 * 			| 	then yCoordinate = Math.abs(yCoordinate)
	 * @post	if the parameter xCoordinate is bigger than the width of the world, the xCoordinate equals to
	 * 			the width of the world.
	 * 			| if(xCoordinate > getWidth())
	 * 			|	then xCoordinate = getWidth()
	 * @post	if the parameter yCoordinate is bigger than the height of the world, the yCoordinate equals to
	 * 			the height of the world.
	 * 			| if(yCoordinate > getHeight())
	 * 			|	then yCoordinate = getHeight()
	 * @return	The result is true if the given region is impassable, false otherwise.
	 * 			| result == !isPassable(xCoordinate, yCoordinate, radius)
	 */
	public boolean isImpassable(double xCoordinate, double yCoordinate, double radius){
		if(xCoordinate < 0)
			xCoordinate = Math.abs(xCoordinate);
		if(yCoordinate < 0)
			yCoordinate = Math.abs(yCoordinate);
		if(xCoordinate > getWidth())
			xCoordinate = getWidth();
		if(yCoordinate >getHeight())
			yCoordinate = getHeight();
		if( radius < 0);
			radius = Math.abs(radius);
		return !isPassable(xCoordinate, yCoordinate, radius);
	}
	
	/**
	 * A method to check whether the given circular region,
	 * defined by the given center coordinates and radius,
	 * is passable.
	 * 
	 * @param	xCoordinate
	 * 			The x-coordinate of the center of the circle to check.
	 * @param	yCoordinate
	 * 			The y-coordinate of the center of the circle to check.
	 * @param	radius
	 * 			The radius of the circle to check.
	 * @post	if the parameter xCoordinate is smaller than zero, the xCoordinate equals to
	 * 			the absolute value of xCoordinate.
	 * 			| if(xCoordinate < 0)
	 * 			| 	then xCoordinate = Math.abs(xCoordinate)
	 * @post	if the parameter yCoordinate is smaller than zero, the yCoordinate equals to
	 * 			the absolute value of yCoordinate.
	 * 			| if(yCoordinate < 0)
	 * 			| 	then yCoordinate = Math.abs(yCoordinate)
	 * @post	if the parameter xCoordinate is bigger than the width of the world, the xCoordinate equals to
	 * 			the width of the world.
	 * 			| if(xCoordinate > getWidth())
	 * 			|	then xCoordinate = getWidth()
	 * @post	if the parameter yCoordinate is bigger than the height of the world, the yCoordinate equals to
	 * 			the height of the world.
	 * 			| if(yCoordinate > getHeight())
	 * 			|	then yCoordinate = getHeight()
	 * @return	The result is true if the center coordinates are passable 
	 * 			and if the given circular region, defined by the given center 
	 * 			coordinates and radius, is passable.
	 * 			| if( getPassable()[yCoordinate][xCoordinate] )
	 * 			|	then for each I in 0..359:
	 * 			|			if( ! (getPassable()[((Math.sin(Math.toRadians(i))*radius) + yCoordinate)]
	 * 			|								[((Math.cos(Math.toRadians(i))*radius) + xCoordinate)]) )
	 * 			|				then result == false;
	 * 			|		result == true;
	 * 			| else 
	 * 			|	result == false;
	 */
	public boolean isPassable(double xCoordinate, double yCoordinate, double radius){
		if(xCoordinate < 0)
			xCoordinate = Math.abs(xCoordinate);
		if(yCoordinate < 0)
			yCoordinate = Math.abs(yCoordinate);
		if(xCoordinate > getWidth())
			xCoordinate = getWidth();
		if(yCoordinate >getHeight())
			yCoordinate = getHeight();
		if( radius < 0);
			radius = Math.abs(radius);
		int row = (int)Math.round( (getPassable().length-1) - (yCoordinate * ((getPassable().length-1)/getHeight())) );
		int column = (int)Math.round( xCoordinate * ((getPassable()[0].length-1)/getWidth()));
		if( ( getPassable()[row][column] ) ){
			for(int i = 0; i < 360; i++ ){																												//-1
				int tempRow = (int)Math.ceil( (getPassable().length-1) - ( ((Math.sin(Math.toRadians(i))*radius) + yCoordinate) * ((getPassable().length)/getHeight())) );
				int tempColumn = (int)Math.floor( ((Math.cos(Math.toRadians(i))*radius) + xCoordinate) * ((getPassable()[0].length)/getWidth()));
				if(tempColumn < 0)
					tempColumn = Math.abs(tempColumn);
				if(tempRow < 0)
					tempRow = Math.abs(tempRow);
				if(tempColumn >= getPassable()[0].length)
					tempColumn = getPassable()[0].length-1;
				if(tempRow >= getPassable().length)
					tempRow = getPassable().length-1;
				if( !( getPassable()[tempRow][tempColumn] ) ){
					return false;
				}
			}
			return true;
		} else{
			return false;
		}
	}
	 
	/**
	 * A method to find a random adjacent location.
	 * 
	 * @param	radius
	 * 			The radius to find a random adjacent location.
	 * @post	If the result is true and only if it is true,
	 * 			the new adjacentLocationX is set to found adjacentLocationX
	 * 			| new.getAdjacentLocationX() == adjacentLocationX
	 * @post	If the result is true and only if it is true,
	 * 			the new adjacentLocationY is set to found adjacentLocationY
	 * 			| new.getAdjacentLocationY() == adjacentLocationY
	 * @return	The result is true if a random perimeter location is found 
	 * 			and a random adjacent location is found.
	 */
	public boolean findRandomAdjacentLocation(double radius){
		if(findRandomPerimeterLocation()){
			double distanceBetweenWormAndMidle = Math.sqrt( Math.pow(this.getPerimeterLocationX() - (this.getWidth()/2), 2) +
					  										Math.pow(this.getPerimeterLocationY() - (this.getHeight()/2), 2) );
			double angleBetweenWormAndMidle = Math.atan( ((this.getHeight()/2) - this.getPerimeterLocationY()) / ((this.getWidth()/2) - this.getPerimeterLocationX()) );
			double x = getPerimeterLocationX();
			double y = getPerimeterLocationY();
			if(getPerimeterLocationX()<getWidth()/2){
				if(angleBetweenWormAndMidle == Math.PI)
					angleBetweenWormAndMidle = 0.0;
				while( !this.isAdjacentToImpassableTerrain(x, y, radius) && distanceBetweenWormAndMidle >= 0.5){
					x = x + (Math.cos(angleBetweenWormAndMidle)*0.01);
					y = y + (Math.sin(angleBetweenWormAndMidle)*0.01);
					distanceBetweenWormAndMidle = Math.sqrt( Math.pow(x - (this.getWidth()/2), 2) +
															 Math.pow(y - (this.getHeight()/2), 2) );
				}
			}
			if(getPerimeterLocationX()>getWidth()/2){
				angleBetweenWormAndMidle = angleBetweenWormAndMidle + Math.PI;
				if(angleBetweenWormAndMidle == 0.0)
					angleBetweenWormAndMidle = Math.PI;
				while( !this.isAdjacentToImpassableTerrain(x, y, radius) && distanceBetweenWormAndMidle >= 0.5){
					x = x + (Math.cos(angleBetweenWormAndMidle)*0.01);
					y = y + (Math.sin(angleBetweenWormAndMidle)*0.01);
					distanceBetweenWormAndMidle = Math.sqrt( Math.pow(x - (this.getWidth()/2), 2) +
															 Math.pow(y - (this.getHeight()/2), 2) );
				}
			}
			if(this.isAdjacentToImpassableTerrain(x, y, radius)){
				adjacentLocationX = x;
				adjacentLocationY = y;
				return true;
			}
			return findRandomAdjacentLocation(radius);
		}
		return findRandomAdjacentLocation(radius);
	}
	
	
	/**
	 * A method to find a random perimeter location.
	 * 
	 * @post	If the result is true and only if it is true,
	 * 			the new perimeterLocationX is set to found perimeterLocationX
	 * 			| new.getPerimeterLocationX() == perimeterLocationX
	 * @post	If the result is true and only if it is true,
	 * 			the new perimeterLocationY is set to found perimeterLocationY
	 * 			| new.getPerimeterLocationY() == perimeterLocationY
	 * @return	The result is true if a random perimeter location is found.
	 * 			| if(randomNumberBetweenOneAndFour != getOldRandomNumber())
	 *			| 	then if(randomNumberBetweenOneAndFour == 1)
	 *			|			then (perimeterLocationX = random.nextInt((int)getWidth()+1)) && (perimeterLocationY = 0.0) && result == true
	 *			|  	then if(randomNumberBetweenOneAndFour == 2)
	 *			|			then (perimeterLocationX = 0.0) && (perimeterLocationY = random.nextInt((int)getHeight()+1)) && result == true
	 *			|	then if(randomNumberBetweenOneAndFour == 3)
	 *			|       	then (perimeterLocationX = random.nextInt((int)getWidth()+1)) && (perimeterLocationY = getHeight()) && result == true
	 *			|	then if(randomNumberBetweenOneAndFour == 4)
	 *			|			then (perimeterLocationX = getWidth()) && (perimeterLocationY = random.nextInt((int)getHeight()+1)) && result == true
	 *			|	then setOldRandomNumber(randomNumberBetweenOneAndFour)
	 *			| else 
	 *			|	findRandomPerimeterLocation()
	 *			| result == true
	 */
	private boolean findRandomPerimeterLocation(){
		int randomNumberBetweenOneAndFour = getRandom().nextInt(4) + 1;
		if(randomNumberBetweenOneAndFour != getOldRandomNumber()){
			if(randomNumberBetweenOneAndFour == 1){
				perimeterLocationX = getRandom().nextInt((int)getWidth()+1);
				perimeterLocationY = 0.0;
				return true;
			}
			if(randomNumberBetweenOneAndFour == 2){
				perimeterLocationX = 0.0;
				perimeterLocationY = getRandom().nextInt((int)getHeight()+1);
				return true;
			}
			if(randomNumberBetweenOneAndFour == 3){
				perimeterLocationX = getRandom().nextInt((int)getWidth()+1);
				perimeterLocationY = getHeight();
				return true;
			}
			if(randomNumberBetweenOneAndFour == 4){
				perimeterLocationX = getWidth();
				perimeterLocationY = getRandom().nextInt((int)getHeight()+1);
				return true;
			}
			setOldRandomNumber(randomNumberBetweenOneAndFour);
		} else {
			findRandomPerimeterLocation();
		}
		return true;
	}
	
	/**
	 * A method that returns the x-coordinate of a adjacent location.
	 */
	@Basic @Raw
	public double getAdjacentLocationX(){
		return adjacentLocationX;
	}
	
	/**
	 * A method that returns the y-coordinate of a adjacent location. 
	 */
	@Basic @Raw
	public double getAdjacentLocationY(){
		return adjacentLocationY;
	}
	
	/**
	 * A method that returns the x-coordinate of a perimeter location. 
	 */
	@Basic @Raw
	private double getPerimeterLocationX(){
		return perimeterLocationX;
	}
	
	/**
	 * A method that returns the y-coordinate of a perimeter location. 
	 */
	@Basic @Raw
	private double getPerimeterLocationY(){
		return perimeterLocationY;
	}
	
	/**
	 * A method that returns the old random number. 
	 */
	@Basic @Raw
	private int getOldRandomNumber() {
		return oldRandomNumber;
	}

	/**
	 * A method to set the old random number. 
	 * 
	 * @param	oldRandomNumber
	 * 			The provided oldRandomNumber.
	 * @post	The new oldRandomNumber is set to the provided oldRandomNumber.
	 * 			| new.getOldRandomNumber = oldRandomNumber
	 */
	@Raw
	private void setOldRandomNumber(int oldRandomNumber) {
		this.oldRandomNumber = oldRandomNumber;
	}
	
	/**
	 * Variable registering the old random number of this world.
	 */
	private int oldRandomNumber = 0;
	
	/**
	 * Variable registering the y-coordinate of the adjacent location.
	 */
	private double adjacentLocationY;
	
	/**
	 * Variable registering the x-coordinate of the adjacent location.
	 */
	private double adjacentLocationX;
	
	/**
	 * Variable registering the x-coordinate of the perimeter location.
	 */
	private double perimeterLocationX;
	
	/**
	 * Variable registering the y-coordinate of the adjacent location.
	 */
	private double perimeterLocationY;
	
	/**
	 * Variable registering a rectangular matrix indicating which parts of the terrain are passable and impassable.
	 */
	private boolean[][] passableMap;
	
	
	//<---------------------------------------------------------------GAME------------------------------------------------------------>
	
	/**
	 * A method to start a new game.
	 */
	public void startGame() {
		if(!isValidToStart())
			throw new IllegalStateException("This world needs more than 1 worm");
		setGameStarted(true);
		setCurrentWorm(getWorms().get(0));
		if(getCurrentWorm().hasProgram())
			getCurrentWorm().getProgram().execute();
	}
	
	/**
	 * A method to check whether the world has more than one worm.
	 *
	 * @return	Result is true if this world has more than one worm.
	 * 			| result == (getWorms().size() > 2)
	 */
	public boolean isValidToStart(){
		return (getWorms().size() >= 2);
	}
	
	/**
	 * A method to check whether the game is finished.
	 * 
	 * @return	If there is only one worm alive then the game
	 * 			is finished.
	 * 			| result == (getWorms().size() == 1)
	 * @return 	If there are more than one worm alive but 
	 * 			they are in the same team then the game is
	 * 			finished.
	 * 			| for each I in 0..getWorms().size()-1:
	 * 			|	if( getWorms().get(0).getTeam() != getWorms().get(i).getTeam() )
	 * 			|		then result == false
	 * 			| result == true
	 */
	public boolean isGameFinished(){
		if(getWorms().size() == 1)
			return true;
		for(int i = 0; i < getWorms().size(); i++){
			if(getWorms().get(0).getTeam() != null){
				if(getWorms().get(0).getTeam() != getWorms().get(i).getTeam()){
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * A method to start the next turn in this world.
	 * 
	 * @post 	If There are still active projectiles in this world then these projectiles
	 * 			will be terminated.
	 * @post	If the action points of the current worm is lower or equal to zero or 
	 * 			the current worm his program is finished or it has executed 1000 statements,
	 * 			then the next worm will be selected. If the current worm was the last worm
	 * 			of the worms of this world, then the first worm will be selected. If this worm
	 * 			has a program it will be executed. 
	 * 			| if(getCurrentWorm().getCurrentActionPoints() <= 0 || getCurrentWorm().getProgram().getNumberOfExecutedStatements >= 1000)
	 * 			|	then if(getWorms().indexOf(getCurrentWorm())+1 >= getWorms().size())
	 * 			|			setCurrentWorm(getWorms().get(0))
	 * 			|				if(getCurrentWorm().hasProgram())
	 * 			|					getCurrentWorm().getProgram().execute()
	 * @post	If the action points of the current worm is lower or equal to zero or 
	 * 			the current worm his program is finished or it has executed 1000 statements,
	 * 			then the next worm will be selected. If the current worm was not the last worm
	 * 			of the worms of this world, then the next worm will be selected. If this worm
	 * 			has a program it will be executed. 
	 * 			| if(getCurrentWorm().getCurrentActionPoints() <= 0)
	 * 			|	then if( !(getWorms().indexOf(getCurrentWorm())+1 >= getWorms().size()))
	 * 			|			setCurrentWorm(getWorms().get(getWorms().indexOf(getCurrentWorm())+1))
	 * 			|				if(getCurrentWorm().hasProgram())
	 * 			|					getCurrentWorm().getProgram().execute()
	 * @post 	The new selected worm, his action points are set to the maximum and his hit points
	 * 			are increased by 10.
	 * 			| new.getCurrentWorm().setCurrentActionPoints(getCurrentWorm().getMaximumHitPoints())
	 * 			| new.getCurrentWorm().setCurrentHitPoints(getCurrentWorm().getCurrentHitPoints()+10)
	 */
	public void startNextTurn(){
		for(Projectile pr : getProjectiles()){
			pr.terminate();
		}
		if(getCurrentWorm().getCurrentActionPoints() <= 0 || (getCurrentWorm().hasProgram() && getCurrentWorm().getProgram().getNumberOfExecutedStatements() >= 1000)){
			if(getWorms().indexOf(getCurrentWorm())+1 >= getWorms().size()){
				setCurrentWorm(getWorms().get(0));
				getCurrentWorm().setCurrentActionPoints(getCurrentWorm().getMaximumHitPoints());
				getCurrentWorm().setCurrentHitPoints(getCurrentWorm().getCurrentHitPoints()+10);
				if(getCurrentWorm().hasProgram()){
					getCurrentWorm().getProgram().execute();
				}
			}
			else{
				setCurrentWorm(getWorms().get(getWorms().indexOf(getCurrentWorm())+1));
				getCurrentWorm().setCurrentActionPoints(getCurrentWorm().getMaximumHitPoints());
				getCurrentWorm().setCurrentHitPoints(getCurrentWorm().getCurrentHitPoints()+10);
				if(getCurrentWorm().hasProgram()){
					getCurrentWorm().getProgram().execute();
				}
			}
		}
	}
	
	/**
	 * A method to return the name of the worm of the winner of this game
	 * or the team name of the worms of the winners of this game.
	 */
	public String getWinner() {
		if(getWorms().get(0).getTeam() != null)
			return getWorms().get(0).getTeam().getName();
		else {
			return getWorms().get(0).getName();
		}
	}
	
	/**
	 * A method to return the boolean gameStarted, if the game is started true else false. 
	 */
	@Basic @Raw
	public boolean isGameStarted() {
		return gameStarted;
	}
	
	/**
	 * A method to set the boolean gameStarted.
	 * 
	 * @param 	gameStarted
	 * 			The new value for the boolean gamestarted.	
	 * @post	The boolean gameStarted is set to the provided gameStarted.
	 * 			| new.isGameStarted() == gameStarted
	 */
	@Basic @Raw
	public void setGameStarted(boolean gameStarted) {
		this.gameStarted = gameStarted;
	}
	
	/**
	 * Variable registering the boolean gameStarted of this world.
	 */
	private boolean gameStarted;
	
}
