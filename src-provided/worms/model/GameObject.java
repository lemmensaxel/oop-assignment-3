package worms.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class of GameObjects.
 * 
 * @invar	Each GameObject must have a valid x-coordinate.
 * 			| isValidCoordinate(getCoordinateX())
 * @invar 	Each GameObject must have a valid y-coordinate.
 * 			| isValidCoordinate(getCoordinateY())
 * @invar  	Each GameObject must have a proper world.
 *       	| hasProperWorld()
 *
 * @version	1.0
 * @author Axel Lemmens & Maarten Rimaux 
 *
 */
public abstract class GameObject {
	
	/**
	 * 
	 * @param 	xCoordinate
	 * 			The provided value for the x-coordinate in meters.
	 * @param 	yCoordinate
	 * 			The provided value for the y-coordinate in meters.
	 * @param 	world
	 * 			The provided world for this GameObject.
	 * @effect	The new value of the x-coordinate is set to the provided value "xCoordinate"
	 * 			| setCoordinateX(xCoordinate)
	 * @effect	The new value of the y-coordinate is set to the provided value "yCoordinate"
	 * 			| setCoordinateY(yCoordinate)
	 * @effect	The new world of this GameObject is set to the provided "world"
	 * 			| setWorld(world)
	 * @post   	The number of GameObjects for the given world is
	 *         	incremented by 1.
	 *       	| (new world).getGameObjects().size() == world.getGameObjects().size() + 1
	 * @post   	The given world has this new GameObject as its very last GameObject.
	 *      	| (new world).getGameObjects().get(world.getGameObjects().size()+1) == this
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided value is not a valid coordinate.
	 * 			| ! isValidCoordinate(xCoordinate)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided value is not a valid coordinate.
	 * 			| ! isValidCoordinate(yCoordinate)
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided world is not a valid world.
	 * 			| ! canHaveAsWorld(world)
	 */
	public GameObject(double xCoordinate, double yCoordinate, World world) throws IllegalArgumentException {
		this.setXCoordinate(xCoordinate);
		this.setYCoordinate(yCoordinate);
		this.setWorld(world);
		world.addGameObject(this);
	}
	
		// POSITION
		
	/**
	* A method to check whether a coordinate is valid.
	* 
	* @param 	aCoordinate
	* 			The coordinate to check.
	* @return 	The result should be a valid number.
	* 			| result == !Double.isNaN(aCoordinate) 
	* 			| && (aCoordinate < Double.POSITIVE_INFINITY)
	* 			| && (aCoordinate > Double.NEGATIVE_INFINITY)
	*/
	public static boolean isValidCoordinate(double aCoordinate) { 
		return (!Double.isNaN(aCoordinate) 
				&& (aCoordinate < Double.POSITIVE_INFINITY) 
				&& (aCoordinate > Double.NEGATIVE_INFINITY));									 
	}
	
	/**
	 * A method to return the x-coordinate of this GameObject item.
	 */
	@Basic @Raw
	public double getCoordinateX() {
		return xCoordinate;
	}
	
	/**
	 * A method to return the y-coordinate of this GameObject item.
	 */
	@Basic @Raw
	public double getCoordinateY() {
		return yCoordinate;
	}
	
	/**
	 * A method to set the x-coordinate of the position of this GameObject item.
	 * 
	 * @param	xCoordinate
	 * 			The new x-coordinate of the position of this GameObject.
	 * @post	The new value of the xCoordinate is set to the provided value.
	 * 			| new.getCoordinateX() == xCoordinate
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided value is not a valid coordinate.
	 * 			| ! isValidCoordinate(xCoordinate) 
	 */
	@Raw
	public void setXCoordinate(double xCoordinate) throws IllegalArgumentException {
		if (! isValidCoordinate(xCoordinate))
			throw new IllegalArgumentException();
		this.xCoordinate = xCoordinate;
	}
	
	/**
	 * A method to set the y-coordinate of the position of this GameObject.
	 * 
	 * @param 	yCoordinate
	 * 			The new y-coordinate of the position of this GameObject.
	 * @post	The new value of the yCoordinate is set to the provided value.
	 * 			| new.getCoordinateY() == yCoordinate
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided value is not a valid coordinate.
	 * 			| ! isValidCoordinate(yCoordinate)
	 */
	@Raw
	public void setYCoordinate(double yCoordinate) throws IllegalArgumentException { 
		if (! isValidCoordinate(yCoordinate))
			throw new IllegalArgumentException();	
		this.yCoordinate = yCoordinate;
		
	}
	
	/**
	 * Variable registering the x-coordinate of the position of this GameObject.
	 */
	private double xCoordinate;
	
	/**
	 * Variable registering the y-coordinate of the position of this GameObject.
	 */
	private double yCoordinate;
	
	
		// WORLD
	
	/**
	 * Check whether this GameObject can have the given world as its current world.
	 * 
	 * @param	world
	 *          The world to check.
	 * @return	If this GameObject is not yet terminated, true if and
	 *          only if the given world is effective.
	 *        	| if (! isTerminated())
	 *       	|   then result == (world != null) 
	 * @return	If this GameObject is terminated, true if and only if
	 *          the given world is not effective.
	 *        	| if (this.isTerminated())
	 *       	|   then result == (world == null)
	 */
	@Raw
	public boolean canHaveAsWorld(World world) {
		if (isTerminated())
			return (world == null);
		return (world != null);
	}
	
	/** 
	 * Return the current world of this GameObject item.
	 */
	@Basic @Raw
	public World getWorld() {
		return this.world;
	}
	
	/** 
	 * Register the given world as the world of this GameObject.
	 * 
	 * @param	world
	 *          The world to be registered as the world of this GameObject.
	 * @post   	The world of this GameObject is the same as the given world.
	 *       	| new.getWorld() == world
	 * @throws	IllegalArgumentException
	 * 			An exception is thrown when the provided world is not valid.
	 * 			| ! canHaveAsWorld(world)
	 */
	@Raw
	public void setWorld(World world) throws IllegalArgumentException {
		if( !canHaveAsWorld(world) )
			throw new IllegalArgumentException("The provided world is not a valid world.");
		this.world = world;
	}
	
	/**
	 * Check whether this GameObject item has a proper world.
	 * 
	 * @return	True if and only if this GameObject can have its world as its
	 * 			world, and if this GameObject is terminated or the world of
	 *          this GameObject has this GameObject as one of its GameObject.
	 *       	| result ==
	 *        	|   canHaveAsWorld(getWorld()) &&
	 *       	|   ( isTerminated() || getWorld().hasAsGameObject(this))
	 */
	public boolean hasProperWorld() {
		return canHaveAsWorld(getWorld()) &&
				 ( isTerminated() || getWorld().hasAsGameObject(this));
	}
	
	/**
	 * Variable referencing the world of this GameObject.
	 */
	private World world;
	
	
		// TERMINATE
	
	/**
	 * Check whether this GameObject item is terminated.
	 */
	@Basic @Raw
	public boolean isTerminated() {
		return this.getState() == State.TERMINATED;
	}
	
	/**
	 * Terminate this GameObject.
	 * 
	 * @post	This GameObject is terminated.
	 *       	| new.isTerminated()
	 * @post   	If this GameObject was not yet terminated, this GameObject
	 *        	is no longer one of the GameObjects of the world to which
	 *        	this GameObject belonged.
	 *     		| if (! isTerminated())
	 *    	    |   then ! (new getWorld()).hasAsGameObject(this))
	 * @post  	If this GameObject was not yet terminated, the number of
	 *      	GameObject of the world to which this GameObject belonged is
	 *        	decremented by 1.
	 *     		| if (! isTerminated())
	 *      	|   then (new getWorld()).getGameObject().size() ==
	 *      	|            this.getWorld().getGameObject().size() - 1
	 * @post  	If this GameObject was not yet terminated, all GameObjects
	 *        	of the world to which this GameObject belonged registered at an
	 *        	index beyond the index at which this GameObject was registered,
	 *        	are shifted one position to the left.
	 *       	| for each I,J in ...getWorld().getGameObject().size()-1:
	 *       	|   if ( (getWorld().getGameObject().get(I) == GameObject) and (I < J) )
	 *       	|     then (new getWorld()).getGameObject().get(J-1) == getWorld().getGameObject().get(J)
	 * 
	 */
	public void terminate() {
		if (!isTerminated()) {
			setState(State.TERMINATED);
			World oldWorld = getWorld();
			this.setWorld(null);
			oldWorld.removeGameObject(this);
		}
	}
	
		// STATE
	/**
	 * Enumeration of all possible states of this GameObject item.
	 */
	protected static enum State {
		ACTIVE, TERMINATED;
	}
	
	/**
	 * Return the state of this GameObject item.
	 */
	@Raw
	protected State getState() {
		return this.state;
	}
	
	/**
	 * Set the state of this GameObject item to the given state.
	 * 
	 * @param	state
	 *        	The new state for this GameObject.
	 * @pre		The given state must exists.
	 *       	| state != null
	 * @post	The state of this GameObject is the same as the given state.
	 *     		| new.getState() == state
	 */
	protected void setState(State state) {
		assert (state != null);
		this.state = state;
	}
	
	/**
	 * Variable registering the state of this GameObject.
	 */
	private State state = State.ACTIVE;

}
